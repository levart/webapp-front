import {Column} from '../../core/model/column';
import {FormlyFieldConfig} from '@ngx-formly/core';


export const RealizationColumns: Column[] = [
  {
    columnName: 'productName',
    columnTitle: 'COLUMNS.productName',
    columnType: 'string'
  },
  {
    columnName: 'unitType',
    columnTitle: 'COLUMNS.unitType',
    columnType: 'string'
  },
  {
    columnName: 'unitValue',
    columnTitle: 'COLUMNS.unitValue',
    columnType: 'string'
  },
  {
    columnName: 'peymentType',
    columnTitle: 'COLUMNS.peymentType',
    columnType: 'string'
  },
  {
    columnName: 'price',
    columnTitle: 'COLUMNS.price',
    columnType: 'string'
  },
  {
    columnName: 'createdBy',
    columnTitle: 'COLUMNS.createdBy',
    columnType: 'string'
  },
  {
    columnName: 'createdAt',
    columnTitle: 'COLUMNS.createdAt',
    columnType: 'datetime'
  }
];


export const RealizationForm: FormlyFieldConfig[] = [
  {
    fieldGroupClassName: 'display-flex',
    fieldGroup: [
      {
        className: 'flex-1',
        key: 'firstname',
        type: 'wa-input',
        templateOptions: {
          label: 'სახელი',
          placeholder: 'მიუთითეთ სახელი',
          required: true,
        }
      },
      {
        className: 'flex-1',
        key: 'lastname',
        type: 'wa-input',
        templateOptions: {
          label: 'გვარი',
          placeholder: 'მიუთითეთ გვარი',
          required: true,
        }
      },
    ],
  }
];
