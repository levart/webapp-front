import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {RealizationsComponent} from './container/realizations/realizations.component';
import {RealizationAddComponent} from './container/realization-add/realization-add.component';


const routes: Routes = [
  {
    path: '',
    component: RealizationsComponent
  },
  {
    path: 'add',
    component: RealizationAddComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RealizationsRoutingModule { }
