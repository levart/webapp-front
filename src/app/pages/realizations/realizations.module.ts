import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RealizationsRoutingModule } from './realizations-routing.module';
import { RealizationsComponent } from './container/realizations/realizations.component';
import {FlexLayoutModule} from '@angular/flex-layout';
import {SharedModule} from '../../shared/shared.module';
import { RealizationAddComponent } from './container/realization-add/realization-add.component';
import {ReactiveFormsModule} from '@angular/forms';
import {FuseSharedModule} from '../../../@fuse/shared.module';
import {OverlayModule} from '@impactdk/ngx-overlay';
import {CalendarModule} from '../calendar/calendar.module';
import { RealizationAddItemComponent } from './component/realization-add-item/realization-add-item.component';


@NgModule({
  declarations: [RealizationsComponent, RealizationAddComponent, RealizationAddItemComponent],
  imports: [
    CommonModule,
    RealizationsRoutingModule,
    FlexLayoutModule,
    SharedModule,
    FuseSharedModule,
    OverlayModule
  ],
  exports: [
    RealizationAddComponent
  ],
  entryComponents: [
    RealizationAddComponent
  ]
})
export class RealizationsModule { }
