import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormGroup} from '@angular/forms';
import {Observable, Subject} from 'rxjs';
import {EmployeeService} from '../../../../core/service/employee/employee.service';
import {takeUntil} from 'rxjs/operators';
import {UserService} from '../../../../core/service/user.service';

@Component({
  selector: 'app-realization-add-item',
  templateUrl: './realization-add-item.component.html',
  styleUrls: ['./realization-add-item.component.scss']
})
export class RealizationAddItemComponent implements OnInit, OnDestroy {

  destroy$: Subject<boolean> = new Subject<boolean>();

  @Input() group;
  employees$: Observable<any>;
  employees;
  users;

  private _realizationAdd;

  get realizationAdd() {
    return this._realizationAdd;
  }

  @Input()
  set realizationAdd(value) {
    this._realizationAdd = value;
    if (this._realizationAdd) {
      this.formData.push(
        this.fb.group({
          productId: this._realizationAdd.id,
          productName: this._realizationAdd.name,
          productMeta: this._realizationAdd,
          paymentType: 1,
          unitGram: null,
          unitQuantity: null,
          userId: null,
          employeeMeta: this.fb.array([]),
          salePercent: null,
          price: this._realizationAdd.salePricePerItem,
          description: null,
        })
      );
    }
  }


  get formData() {
    return <FormArray> this.group.get('realizations');
  }

  constructor(
    private fb: FormBuilder,
    private service: EmployeeService,
    private userService: UserService,
  ) {
  }

  ngOnInit() {
    this.getEmployees();
    this.getUsers();
  }

  getEmployees() {
    this.service.getDropdown()
      .pipe(takeUntil(this.destroy$))
      .subscribe(res => {
        this.employees = res;
        console.log(this.employees);
      });
  }

  getUsers() {
    this.userService.getAll()
      .pipe(takeUntil(this.destroy$))
      .subscribe(res => {
        this.users = res;
        console.log(this.users);
      });
  }

  getEmployeeMeta(item) {
    return item.controls.employeeMeta.controls;
  }

  addEmployeeMeta(i) {
    console.log(i);
    const control = <FormArray> this.formData.controls[i].get('employeeMeta');
    // console.log(control);
    control.push(
      this.fb.group({
        employeeId: null,
        employeePercent: null,
        employeFixedPrice: null
      })
    );
    console.log(control.value);
  }


  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }

  deleteEmployeeMeta(i: number) {

  }
}
