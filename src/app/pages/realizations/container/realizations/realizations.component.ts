import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {FuseSidebarService} from '../../../../../@fuse/components/sidebar/sidebar.service';
import {MatDialog} from '@angular/material';
import {ConfirmationDialogComponent} from '../../../../shared/components/confirmation-dialog/confirmation-dialog.component';
import {RealizationService} from '../../../../core/service/realization.service';
import {RealizationColumns} from '../../realization-columns.data';
import {OverlayService} from '@impactdk/ngx-overlay';
import {RealizationAddComponent} from '../realization-add/realization-add.component';

@Component({
  selector: 'app-realizations',
  templateUrl: './realizations.component.html',
  styleUrls: ['./realizations.component.scss']
})
export class RealizationsComponent implements OnInit {

  isLoading = false;
  loadData$: Observable<any>;
  displayedColumns = RealizationColumns;
  paginatorPageSize = 10;
  paginatorPageIndex = 1;
  paymentType = 0;

  constructor(
    private fuseSidebarService: FuseSidebarService,
    private service: RealizationService,
    public dialog: MatDialog,
    private overlay: OverlayService
  ) {
  }

  ngOnInit() {
    this.getProducts();
  }

  private getProducts() {
    const params = {
      paymentType: this.paymentType,
      page: this.paginatorPageIndex,
      pageSize: this.paginatorPageSize
    };
    this.loadData$ = this.service.get(params);
  }

  paginatorChange(pageEvent) {
    this.paginatorPageSize = pageEvent.pageSize;
    this.paginatorPageIndex = pageEvent.page;
    this.getProducts();
  }

  delete(event) {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '600px',
      data: {
        message: 'ნამდვილად გსურთ წაშლა?',
        title: 'კატეგორიის წაშლა'
      }
    });
    return dialogRef.afterClosed()
      .toPromise()
      .then((result: boolean) => {
        if (result) {
          this.service.delete(event)
            .pipe()
            .subscribe(res => {
              this.getProducts();
            });
        }
      });
  }

  paymentTypeSelect($event) {
    this.paymentType = $event;
    this.getProducts();
  }

  addRealization(event: MouseEvent) {
    const overlayRef = this.overlay.open(RealizationAddComponent);

    overlayRef
      .afterClose()
      .subscribe(response => {
        if (response) {
          this.getProducts();
        }
      });
  }
}
