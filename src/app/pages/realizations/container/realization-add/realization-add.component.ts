import {ChangeDetectorRef, Component, Inject, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatInput} from '@angular/material';
import {FormBuilder, FormGroup} from '@angular/forms';
import {ProductService} from '../../../../core/service/product/product.service';
import {Observable, of, Subject} from 'rxjs';
import {CustomerService} from '../../../../core/service/customer.service';
import {RealizationService} from '../../../../core/service/realization.service';
import {EmployeeService} from '../../../../core/service/employee/employee.service';
import {IMPACT_OVERLAY_DATA, ImpactOverlayRef} from '@impactdk/ngx-overlay';
import {takeUntil} from 'rxjs/operators';


export interface PeriodicElement {
  name: string;
  unitType: number;
  unitValue: number;
  peymentType: string;
  employeePercent: string;
  employeFixedPrice: string;
}

const ELEMENT_DATA: PeriodicElement[] = [];

@Component({
  selector: 'app-realization-add',
  templateUrl: './realization-add.component.html',
  styleUrls: ['./realization-add.component.scss']
})
export class RealizationAddComponent implements OnInit , OnDestroy {

  destroy$: Subject<boolean> = new Subject<boolean>();

  @ViewChild('searchInput', {static: false}) searchInput: MatInput;
  @ViewChild('CustomerSearchInput', {static: false}) CustomerSearchInput: MatInput;

  addForm: FormGroup;
  searchForm: FormGroup;
  productSerachForm: FormGroup;

  realizationAdd$: Observable<any>;
  options$: Observable<any>;
  customers$: Observable<any>;
  employees$: Observable<any>;

  items = [];

  totalPrice = 0;

  displayedColumns = [
    'productId',
    'realizationType',
    'unitValue',
    'unitType',
    'employeePercent',
    'employeFixedPrice',
    'salePercent',
    'price'
  ];
  dataSource = [];


  constructor(
    public overlayRef: ImpactOverlayRef,
    @Inject(IMPACT_OVERLAY_DATA) public data: any,
    private productService: ProductService,
    private customerService: CustomerService,
    private emplService: EmployeeService,
    private realizationService: RealizationService,
    private fb: FormBuilder,
    private cd: ChangeDetectorRef
  ) {

    this.searchForm = this.fb.group({
      search: null,
      userId: null
    });

    this.productSerachForm = this.fb.group({
      search: null,
      userid: null
    });
  }

  ngOnInit() {
    this.addForm = this.fb.group({
      realizations: this.fb.array([]),
    });
    this.getEmployees();
    // this.addForm.valueChanges.subscribe(input => {
    //   if (input.productId) {
    //     this.productSerach(input.productId);
    //   }
    // });

    // this.productSerachForm.valueChanges.subscribe(input => {
    //   console.log(input);
    //
    // });

  }

  getEmployees() {
    this.employees$ = this.emplService.getDropdown();
    this.customers$ = this.customerService.getAll();
  }

  productSerach(search) {
    this.options$ = this.productService.search(search);
  }

  customerSerach(search) {
    this.customers$ = this.customerService.search(search);
  }


  selectedSerach(option: any) {
    console.log(option);
    this.addForm.patchValue({product: option, price: option.salePricePerItem});
    this.cd.markForCheck();
  }

  deleteItem(item) {
    const findIndex = this.dataSource.indexOf(item);
    this.dataSource = [
      ...this.dataSource.slice(0, findIndex),
      ...this.dataSource.slice(findIndex + 1)
    ];
  }

  addItem() {
    console.log(this.addForm.value);
    this.items = [
      ...this.items,
      this.addForm.value
    ];

    this.totalPrice = 0;
    this.items.forEach(item => this.totalPrice += item.price);
  }

  realization() {
    this.realizationService.create(this.items).subscribe(item => console.log(item));
  }

  chooseProduct($event: any) {
    console.log($event);
    this.realizationAdd$ = of($event);
  }


  submit() {
    console.log(this.addForm.value);
    if (this.addForm.valid) {
      this.realizationService.create(this.addForm.value)
        .pipe(takeUntil(this.destroy$))
        .subscribe(res => {
          if (res) {
            console.log(res);
            this.overlayRef.close(res);
          }
        });
    }
  }

  closeSidebar() {
    this.overlayRef.close();
  }

  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }
}
