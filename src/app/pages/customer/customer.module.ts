import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CustomerRoutingModule } from './customer-routing.module';
import { CustomerComponent } from './components/customer/customer.component';
import {SharedModule} from '../../shared/shared.module';
import {FuseSharedModule} from '../../../@fuse/shared.module';
import {NgxsModule} from '@ngxs/store';
import {OverlayModule} from '@impactdk/ngx-overlay';
import {MatDialogModule, MatTabsModule} from '@angular/material';
import {TranslateModule} from '@ngx-translate/core';
import {FormlyModule} from '@ngx-formly/core';
import { CustomerSidebarComponent } from './components/customer-sidebar/customer-sidebar.component';
import { CustomerViewComponent } from './components/customer-view/customer-view.component';

@NgModule({
  declarations: [CustomerComponent, CustomerSidebarComponent, CustomerViewComponent],
  imports: [
    CommonModule,
    CustomerRoutingModule,
    SharedModule,
    FuseSharedModule,
    NgxsModule.forFeature(),
    OverlayModule,
    TranslateModule.forChild(),
    FormlyModule.forChild()
  ],
  entryComponents: [CustomerSidebarComponent, CustomerViewComponent]
})
export class CustomerModule { }
