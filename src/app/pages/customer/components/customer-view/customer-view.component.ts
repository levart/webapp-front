import {Component, Inject, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {IMPACT_OVERLAY_DATA, ImpactOverlayRef} from '@impactdk/ngx-overlay';
import {EmployeeService} from '../../../../core/service/employee/employee.service';
import {CustomerService} from '../../../../core/service/customer.service';

@Component({
  selector: 'app-customer-view',
  templateUrl: './customer-view.component.html',
  styleUrls: ['./customer-view.component.scss']
})
export class CustomerViewComponent implements OnInit {

  data$: Observable<any>;

  constructor(
    public overlayRef: ImpactOverlayRef,
    @Inject(IMPACT_OVERLAY_DATA) public data: string,
    private service: CustomerService
  ) {
  }

  ngOnInit() {
    this.getEmployee(this.data);
  }

  getEmployee(id) {
    this.data$ = this.service.getById(id);
  }

  closeSidebar() {
    this.overlayRef.close();
  }
}
