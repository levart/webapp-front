import {Component, Inject, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {FormlyFieldConfig} from '@ngx-formly/core';
import {ProductForm} from '../../../products/components/products-list/products-columns.data';
import {IMPACT_OVERLAY_DATA, ImpactOverlayRef} from '@impactdk/ngx-overlay';
import {ProductService} from '../../../../core/service/product/product.service';
import {CustomerService} from '../../../../core/service/customer.service';
import {CustomerForm} from '../../customer-columns.data';

@Component({
  selector: 'app-customer-sidebar',
  templateUrl: './customer-sidebar.component.html',
  styleUrls: ['./customer-sidebar.component.scss']
})
export class CustomerSidebarComponent implements OnInit {

  form = new FormGroup({});
  model = {};
  fields: FormlyFieldConfig[] = CustomerForm;


  constructor(
    public overlayRef: ImpactOverlayRef,
    @Inject(IMPACT_OVERLAY_DATA) public data: any,
    private service: CustomerService,
  ) {
    if (this.data.isEdit) {
      this.model = {
        ...this.data.customer,
      };
    }

  }

  ngOnInit() {
  }

  submit(model) {
    if (this.form.valid) {
      if (this.data.isEdit) {
        this.service.update(model)
          .subscribe(res => {
            this.overlayRef.close(true);
          });
      } else {
        this.service.create(model)
          .subscribe(res => {
            this.overlayRef.close(true);
          });
      }
    }
  }

  closeSidebar() {
    this.overlayRef.close();
  }

}
