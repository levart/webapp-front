import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {FuseSidebarService} from '../../../../../@fuse/components/sidebar/sidebar.service';
import {OverlayService} from '@impactdk/ngx-overlay';
import {MatDialog} from '@angular/material';
import {PageParams} from '../../../../core/model/page-param';
import {ConfirmationDialogComponent} from '../../../../shared/components/confirmation-dialog/confirmation-dialog.component';
import {UserService} from '../../../../core/service/user.service';
import {CustomerSidebarComponent} from '../customer-sidebar/customer-sidebar.component';
import {CustomerColumns} from '../../customer-columns.data';
import {EmployeeViewComponent} from '../../../employees/components/employee-view/employee-view.component';
import {CustomerViewComponent} from '../customer-view/customer-view.component';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.scss']
})
export class CustomerComponent implements OnInit {

  isLoading = false;
  loadData$: Observable<any>;
  displayedColumns = CustomerColumns;
  paginatorPageSize = 10;
  paginatorPageIndex = 1;

  constructor(
    private fuseSidebarService: FuseSidebarService,
    private userService: UserService,
    private overlay: OverlayService,
    public dialog: MatDialog
  ) {
  }

  ngOnInit() {
    this.getProducts();
  }

  private getProducts() {
    const params: PageParams = {
      page: this.paginatorPageIndex,
      pageSize: this.paginatorPageSize
    };
    this.loadData$ = this.userService.getAll(params);
  }

  paginatorChange(pageEvent) {
    this.paginatorPageSize = pageEvent.pageSize;
    this.paginatorPageIndex = pageEvent.page;
    this.getProducts();
  }

  delete(event) {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '600px',
      data: {
        message: 'ნამდვილად გსურთ წაშლა?',
        title: 'კატეგორიის წაშლა'
      }
    });
    return dialogRef.afterClosed()
      .toPromise()
      .then((result: boolean) => {
        if (result) {
          console.log(result);
          this.userService.delete(event)
            .pipe()
            .subscribe(res => {
              this.getProducts();
            });
        }
      });
  }

  edit(event, isEdit = false) {
    const overlayRef = this.overlay.open(CustomerSidebarComponent, {
      data: {
        customer: event,
        isEdit
      },
      fullHeight: true,
      positionVertical: {placement: 'bottom'},
      positionHorizontal: {placement: 'right'}
    });

    overlayRef
      .afterClose()
      .subscribe(response => {
        if (response) {
          this.getProducts();
        }
      });
  }

  view(event) {
    const overlayRef = this.overlay.open(CustomerViewComponent, {
      data: event.id,
      fullHeight: true,
      positionVertical: {placement: 'bottom'},
      positionHorizontal: {placement: 'right'}
    });

    overlayRef
      .afterClose()
      .subscribe(response => {
        console.log(response);
        if (response) {

        }
      });
  }
}
