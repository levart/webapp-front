import {FormlyFieldConfig} from '@ngx-formly/core';
import {Column} from '../../core/model/column';


export const CustomerColumns: Column[] = [
  {
    columnName: 'firstname',
    columnTitle: 'COLUMNS.firstname',
    columnType: 'string'
  },
  {
    columnName: 'lastname',
    columnTitle: 'COLUMNS.lastname',
    columnType: 'string'
  },
  {
    columnName: 'identityNumber',
    columnTitle: 'COLUMNS.identityNumber',
    columnType: 'string'
  },
  {
    columnName: 'gender',
    columnTitle: 'COLUMNS.gender',
    columnType: 'string'
  },
  {
    columnName: 'birthDate',
    columnTitle: 'COLUMNS.birthDate',
    columnType: 'date'
  },
  {
    columnName: 'phone',
    columnTitle: 'COLUMNS.phone',
    columnType: 'string'
  },
  {
    columnName: 'createdAt',
    columnTitle: 'COLUMNS.createdAt',
    columnType: 'datetime'
  }
];


export const CustomerForm: FormlyFieldConfig[] = [
  {
    fieldGroupClassName: 'display-flex',
    fieldGroup: [
      {
        className: 'flex-1',
        key: 'firstname',
        type: 'wa-input',
        templateOptions: {
          label: 'სახელი',
          placeholder: 'მიუთითეთ სახელი',
          required: true,
        }
      },
      {
        className: 'flex-1',
        key: 'lastname',
        type: 'wa-input',
        templateOptions: {
          label: 'გვარი',
          placeholder: 'მიუთითეთ გვარი',
          required: true,
        }
      },
    ],
  },
  {
    fieldGroupClassName: 'display-flex',
    fieldGroup: [
      {
        className: 'flex-1',
        key: 'mobilePhone',
        type: 'wa-input',
        templateOptions: {
          label: 'მობილურის ნომერი',
          placeholder: 'მობილურის ნომერი',
          required: true,
        }
      },
      {
        className: 'flex-1',
        key: 'phone',
        type: 'wa-input',
        templateOptions: {
          label: 'სახლის ნომერ',
          placeholder: 'სახლის ნომერი'
        }
      },
    ],
  },
  {
    fieldGroupClassName: 'display-flex',
    fieldGroup: [
      {
        className: 'flex-1',
        key: 'identityNumber',
        type: 'wa-input',
        templateOptions: {
          label: 'პირადი ნომერი',
          placeholder: 'პირადი ნომერი',
          required: true,
        }
      },
      {
        className: 'flex-1',
        key: 'birthDate',
        type: 'wa-datepicker',
        templateOptions: {
          label: 'დაბადების დღე',
          placeholder: 'დაბადების დღე',
          required: true,
        }
      },
    ],
  },
  {
    fieldGroupClassName: 'display-flex',
    fieldGroup: [
      {
        className: 'flex-1',
        key: 'gender',
        type: 'select',
        templateOptions: {
          label: 'სქესი',
          placeholder: 'სქესი',
          required: true,
          options: [
            {
              value: 'MALE',
              label: 'მამრობითი'
            },
            {
              value: 'FEMALE',
              label: 'მდედრობითი'
            }
          ]
        }
      },
      {
        className: 'flex-1',
        key: 'email',
        type: 'wa-input',
        templateOptions: {
          label: 'ელ-ფოსტა',
          placeholder: 'ელ-ფოსტა'
        }
      },
    ],
  },
  {
    key: 'description',
    type: 'textarea',
    templateOptions: {
      label: 'დამატებითი ინფო',
      placeholder: 'დამატებითი ინფო'
    }
  }
];
