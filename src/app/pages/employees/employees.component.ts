import {Component, OnDestroy, OnInit} from '@angular/core';
import {FuseSidebarService} from '@fuse/components/sidebar/sidebar.service';
import {PositionService} from 'app/core/service/position/position.service';
import {Observable, Subject} from 'rxjs';
import {ActivatedRoute} from '@angular/router';
import {map, takeUntil} from 'rxjs/operators';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ConfirmationDialogComponent} from '../../shared/components/confirmation-dialog/confirmation-dialog.component';
import {MatDialog} from '@angular/material';
import {EmployeeViewComponent} from './components/employee-view/employee-view.component';
import {OverlayService} from '@impactdk/ngx-overlay';
import {EmployeeAddComponent} from './components/employee-add/employee-add.component';

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.scss']
})
export class EmployeesComponent implements OnInit, OnDestroy {
  private unsubscribe$ = new Subject<void>();

  positions$: Observable<any>;
  positionId;

  addPositionState = false;
  addField: string;

  form: FormGroup;

  constructor(
    private fuseSidebarService: FuseSidebarService,
    private positionService: PositionService,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    public dialog: MatDialog,
    private overlay: OverlayService,
  ) {

    this.form = this.formBuilder.group({
      id: [null],
      name: [null, Validators.required]
    });

  }

  ngOnInit() {
    this.getPositions();
    this.route.queryParams
      .pipe(takeUntil(this.unsubscribe$)).subscribe(queryParams => {
      console.log(queryParams);
      if (queryParams.position) {
        this.positionId = queryParams.position;
      }
    });
  }

  getPositions() {
    this.positions$ = this.positionService.getPositions({})
      .pipe(
        map(item => ({
          ...item,
          canEdit: false
        }))
      );
  }


  toggleSidebar(name): void {
    this.fuseSidebarService.getSidebar(name).toggleOpen();
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  addPosition() {
    console.log('add position');
    this.addPositionState = !this.addPositionState;
  }

  submitPosition() {
    const {name} = this.form.value;
    this.positionService.addPosition(name).pipe()
      .subscribe(res => {
        if (res) {
          this.getPositions();
        }
      });
  }


  deletePosition(event) {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '600px',
      data: {
        message: 'ნამდვილად გსურთ წაშლა?',
        title: 'პოზიციის წაშლა'
      }
    });
    return dialogRef.afterClosed()
      .toPromise()
      .then((result: boolean) => {
        if (result) {
          console.log(result);
          this.positionService.deletePosition(event)
            .pipe()
            .subscribe(res => {
              if (res) {
                this.getPositions();
              }
            });
        }
      });
  }

  editPosition(item: any) {
    item.canEdit = true;

  }

  submitUpdatePosition(event: any) {
    this.positionService.updatePosition(event.id, event).pipe()
      .subscribe(res => {
        if (res) {
          this.getPositions();
        }
      });
  }
}
