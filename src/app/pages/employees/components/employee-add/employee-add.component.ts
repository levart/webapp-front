import {Component, Inject, OnInit} from '@angular/core';
import {IMPACT_OVERLAY_DATA, ImpactOverlayRef} from '@impactdk/ngx-overlay';
import {FormlyFieldConfig} from '@ngx-formly/core';
import {EmployeeForm} from '../../employee-columns.data';
import {FormGroup} from '@angular/forms';
import {EmployeeService} from '../../../../core/service/employee/employee.service';

@Component({
  selector: 'app-employee-add',
  templateUrl: './employee-add.component.html',
  styleUrls: ['./employee-add.component.scss']
})
export class EmployeeAddComponent implements OnInit {
  form = new FormGroup({});
  model = {};
  fields: FormlyFieldConfig[] = EmployeeForm;


  constructor(
    public overlayRef: ImpactOverlayRef,
    @Inject(IMPACT_OVERLAY_DATA) public data: any,
    private employeeService: EmployeeService,
  ) {
    if (this.data.isEdit) {
      this.model = {
        positionId: this.data.employee.position.id,
        ...this.data.employee.user,
        ...this.data.employee,
      };
    }

  }

  ngOnInit() {
  }

  submit(model) {
    if (this.form.valid) {
      if (this.data.isEdit) {
        this.employeeService.update(model)
          .subscribe(res => {
            this.overlayRef.close(true);
          });
      } else {
        this.employeeService.create(model)
          .subscribe(res => {
            this.overlayRef.close(true);
          });
      }
    }
  }

  closeSidebar() {
    this.overlayRef.close();
  }
}
