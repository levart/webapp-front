import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'app-employee-card',
  templateUrl: './employee-card.component.html',
  styleUrls: ['./employee-card.component.scss']
})
export class EmployeeCardComponent implements OnInit {
  @Input() employee;
  @Output() employeeEdit: EventEmitter<string> = new EventEmitter<string>();
  @Output() employeeDelete: EventEmitter<string> = new EventEmitter<string>();

  constructor() {
  }

  ngOnInit() {
  }

  view(employeeId) {
    this.employeeEdit.emit(employeeId);
  }

  delete(employeeId) {
    this.employeeDelete.emit(employeeId);
  }
}
