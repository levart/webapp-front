import {Component, Input, OnInit} from '@angular/core';
import {SalaryService} from '../../../../core/service/salary.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-employee-salary',
  templateUrl: './employee-salary.component.html',
  styleUrls: ['./employee-salary.component.scss']
})
export class EmployeeSalaryComponent implements OnInit {

  @Input() employeeId;
  form: FormGroup;

  constructor(
    private salaryService: SalaryService,
    private fb: FormBuilder
  ) {
    this.form = this.fb.group({
      employeeId: null,
      salaryDate: [null, Validators.required],
      salarySum: [null, Validators.required]
    });
  }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.form.patchValue({employeeId: this.employeeId});
  }

  submit() {
    if (this.form.valid) {
      console.log(this.form.value);
      this.salaryService.create(this.form.value)
        .subscribe(res => {
          if (res) {
            console.log(res);
          }
        });
    }
  }

}
