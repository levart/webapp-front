import {ChangeDetectionStrategy, Component, Input, OnChanges, OnInit, SimpleChange, SimpleChanges} from '@angular/core';
import {Observable} from 'rxjs';
import {EmployeeService} from '../../../../core/service/employee/employee.service';
import {OverlayService} from '@impactdk/ngx-overlay';
import {EmployeeViewComponent} from '../employee-view/employee-view.component';
import {ConfirmationDialogComponent} from '../../../../shared/components/confirmation-dialog/confirmation-dialog.component';
import {MatDialog} from '@angular/material';
import {EmployeeColumns} from '../../employee-columns.data';
import {map, mergeMap, switchMap} from 'rxjs/operators';
import {EmployeeAddComponent} from '../employee-add/employee-add.component';
import {FuseSidebarService} from '../../../../../@fuse/components/sidebar/sidebar.service';

@Component({
  selector: 'app-employees-content',
  templateUrl: './employees-content.component.html',
  styleUrls: ['./employees-content.component.scss']
})
export class EmployeesContentComponent implements OnInit, OnChanges {

  @Input() positionId: string;
  isLoading = false;

  displayedColumns = EmployeeColumns;
  paginatorPageSize = 10;
  paginatorPageIndex = 1;
  employees$: Observable<any>;

  constructor(
    private fuseSidebarService: FuseSidebarService,
    private employeeService: EmployeeService,
    private overlay: OverlayService,
    public dialog: MatDialog,
  ) {
  }

  ngOnInit() {

    this.getEmployees(this.positionId);
  }

  ngOnChanges(changes: SimpleChanges): void {
    const positionId: SimpleChange = changes.positionId.currentValue;
    if (positionId) {
      this.getEmployees(positionId);
    }
  }

  getEmployees(positionId: any) {
    const params = {
      page: this.paginatorPageIndex,
      pageSize: this.paginatorPageSize,
      positionId
    };
    this.employees$ = this.employeeService.getEmployees(params).pipe(
      map(item => {
        return {
          ...item,
          data: item.data.map(m => {
            return {
              ...m.user,
              ...m,
            };
          }),

        };
      })
    );
  }

  paginatorChange(pageEvent) {
    this.paginatorPageSize = pageEvent.pageSize;
    this.paginatorPageIndex = pageEvent.page;
    this.getEmployees(this.positionId);
  }

  delete(event) {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '600px',
      data: {
        message: 'ნამდვილად გსურთ წაშლა?',
        title: 'თანამშრომლის წაშლა'
      }
    });
    return dialogRef.afterClosed()
      .toPromise()
      .then((result: boolean) => {
        if (result) {
          console.log(result);
          this.employeeService.deleteEmployee(event)
            .pipe()
            .subscribe(res => {
              console.log(res);
              this.getEmployees(this.positionId);
            });
        }
      });
  }

  view(event) {
    console.log(event);
    const overlayRef = this.overlay.open(EmployeeViewComponent, {
      data: event.id,
      fullHeight: true,
      positionVertical: {placement: 'bottom'},
      positionHorizontal: {placement: 'right'}
    });

    overlayRef
      .afterClose()
      .subscribe(response => {
        console.log(response);
        if (response) {

        }
      });
  }

  addEmployee(event, isEdit = false) {
    const overlayRef = this.overlay.open(EmployeeAddComponent, {
      data: {
        employee: event,
        isEdit
      },
      fullHeight: true,
      positionVertical: {placement: 'bottom'},
      positionHorizontal: {placement: 'right'}
    });

    overlayRef
      .afterClose()
      .subscribe(response => {
        if (response) {
          this.getEmployees(this.positionId);
        }
      });
  }

  toggleSidebar(name): void {
    this.fuseSidebarService.getSidebar(name).toggleOpen();
  }

}
