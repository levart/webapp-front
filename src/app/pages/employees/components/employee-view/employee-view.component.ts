import {Component, Inject, OnInit} from '@angular/core';
import {IMPACT_OVERLAY_DATA, ImpactOverlayRef} from '@impactdk/ngx-overlay';
import {EmployeeService} from 'app/core/service/employee/employee.service';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-employee-view',
  templateUrl: './employee-view.component.html',
  styleUrls: ['./employee-view.component.scss']
})
export class EmployeeViewComponent implements OnInit {
  employee$: Observable<any>;

  constructor(
    public overlayRef: ImpactOverlayRef,
    @Inject(IMPACT_OVERLAY_DATA) public data: string,
    private employeeService: EmployeeService
  ) {
  }

  ngOnInit() {
    this.getEmployee(this.data);
  }

  getEmployee(employeeId) {
    this.employee$ = this.employeeService.getById(employeeId);
  }

  closeSidebar() {
    this.overlayRef.close();
  }
}
