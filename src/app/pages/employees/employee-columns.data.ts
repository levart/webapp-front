import {Column} from '../../core/model/column';
import {FormlyFieldConfig} from '@ngx-formly/core';


export const EmployeeColumns: Column[] = [
  {
    columnName: 'firstname',
    columnTitle: 'COLUMNS.firstname',
    columnType: 'string'
  },
  {
    columnName: 'lastname',
    columnTitle: 'COLUMNS.lastname',
    columnType: 'string'
  },
  {
    columnName: 'mobilePhone',
    columnTitle: 'COLUMNS.mobilePhone',
    columnType: 'string'
  },
  {
    columnName: 'phone',
    columnTitle: 'COLUMNS.phone',
    columnType: 'string'
  },
  {
    columnName: 'position',
    columnTitle: 'COLUMNS.position',
    columnType: 'object',
    objectName: 'name'
  },
  {
    columnName: 'createdAt',
    columnTitle: 'COLUMNS.createdAt',
    columnType: 'datetime'
  }
];


export const EmployeeForm: FormlyFieldConfig[] = [
  {
    fieldGroupClassName: 'display-flex',
    fieldGroup: [
      {
        className: 'flex-1',
        key: 'firstname',
        type: 'wa-input',
        templateOptions: {
          label: 'სახელი',
          placeholder: 'მიუთითეთ სახელი',
          required: true,
        }
      },
      {
        className: 'flex-1',
        key: 'lastname',
        type: 'wa-input',
        templateOptions: {
          label: 'გვარი',
          placeholder: 'მიუთითეთ გვარი',
          required: true,
        }
      },
    ],
  },
  {
    fieldGroupClassName: 'display-flex',
    fieldGroup: [
      {
        className: 'flex-1',
        key: 'mobilePhone',
        type: 'wa-input',
        templateOptions: {
          label: 'მობილურის ნომერი',
          placeholder: 'მობილურის ნომერი',
          required: true,
        }
      },
      {
        className: 'flex-1',
        key: 'phone',
        type: 'wa-input',
        templateOptions: {
          label: 'სახლის ნომერ',
          placeholder: 'სახლის ნომერი'
        }
      },
    ],
  },
  {
    fieldGroupClassName: 'display-flex',
    fieldGroup: [
      {
        className: 'flex-1',
        key: 'identityNumber',
        type: 'wa-input',
        templateOptions: {
          label: 'პირადი ნომერი',
          placeholder: 'პირადი ნომერი',
          required: true,
        }
      },
      {
        className: 'flex-1',
        key: 'birthDate',
        type: 'wa-datepicker',
        templateOptions: {
          label: 'დაბადების დღე',
          placeholder: 'დაბადების დღე',
          required: true,
        }
      },
    ],
  },
  {
    fieldGroupClassName: 'display-flex',
    fieldGroup: [
      {
        className: 'flex-1',
        key: 'gender',
        type: 'gender',
        templateOptions: {
          label: 'სქესი',
          placeholder: 'სქესი',
          required: true
        }
      },
      {
        className: 'flex-1',
        key: 'email',
        type: 'wa-input',
        templateOptions: {
          label: 'ელ-ფოსტა',
          placeholder: 'ელ-ფოსტა'
        }
      },
    ],
  },
  {
    fieldGroupClassName: 'display-flex',
    fieldGroup: [
      {
        className: 'flex-1',
        key: 'workType',
        type: 'salary-type',
        templateOptions: {
          label: 'ხელფასის ტიპი',
          placeholder: 'ხელფასის ტიპი',
          required: true
        }
      },
      {
        className: 'flex-1',
        key: 'positionId',
        type: 'positions',
        templateOptions: {
          label: 'პოზიცია',
          placeholder: 'პოზიცია',
          required: true,
        }
      },
    ],
  },
  {
    fieldGroupClassName: 'display-flex',
    fieldGroup: [
      {
        className: 'flex-1',
        key: 'fixedSalary',
        type: 'wa-input',
        templateOptions: {
          label: 'ფიქსირებული ხელფასი',
          required: true,
        },
        hideExpression: (model) => {
          if (model.workType === 'ALL') {
            return false;
          }
          if (model.workType !== 'FIXED') {
            return true;
          }
          return false;
        }
      },
      {
        className: 'flex-1',
        key: 'outputPercent',
        type: 'wa-input',
        templateOptions: {
          label: 'გამომუშავებაზე',
          required: true,
        },
        hideExpression: (model) => {
          if (model.workType === 'ALL') {
            return false;
          }
          if (model.workType !== 'OUTPUT') {
            return true;
          }
          return false;
        }
      },
    ],
  },
  {
    fieldGroupClassName: 'display-flex',
    fieldGroup: [
      {
        className: 'flex-1',
        key: 'inCallendar',
        type: 'toggle',
        templateOptions: {
          label: 'გამოჩნდეს კალენდარში'
        }
      },
      {
        className: 'flex-1',
        key: 'visibleInApp',
        type: 'toggle',
        templateOptions: {
          label: 'გამოჩნდეს აპლიკაციაში'
        }
      },
    ],
  },
  {
    key: 'description',
    type: 'textarea',
    templateOptions: {
      label: 'დამატებითი ინფო',
      placeholder: 'დამატებითი ინფო'
    }
  }
];
