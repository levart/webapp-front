import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {EmployeesRoutingModule} from './employees-routing.module';
import {EmployeesComponent} from './employees.component';
import {MatButtonModule} from '@angular/material/button';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatIconModule} from '@angular/material/icon';
import {MatInputModule} from '@angular/material/input';
import {FuseSharedModule} from '@fuse/shared.module';
import {NgxsModule} from '@ngxs/store';
import {FuseSidebarModule} from '@fuse/components';
import {MatDividerModule} from '@angular/material/divider';
import {MatListModule} from '@angular/material/list';
import {EmployeeCardComponent} from './components/employee-card/employee-card.component';
import {EmployeesContentComponent} from './components/employees-content/employees-content.component';
import {EmployeeViewComponent} from './components/employee-view/employee-view.component';
import {OverlayModule} from '@impactdk/ngx-overlay';
import {MatDialogModule, MatTabsModule} from '@angular/material';
import {TranslateModule} from '@ngx-translate/core';
import {SharedModule} from 'app/shared/shared.module';
import {EmployeeAddComponent} from './components/employee-add/employee-add.component';
import {FormlyModule} from '@ngx-formly/core';
import {PositionFormComponent} from './components/position-form/position-form.component';
import {EmployeeSalaryComponent} from './components/employee-salary/employee-salary.component';
import {EmployeeRealizationComponent} from './components/employee-realization/employee-realization.component';

@NgModule({
  declarations: [EmployeesComponent, EmployeeCardComponent, EmployeesContentComponent, EmployeeViewComponent, EmployeeAddComponent, PositionFormComponent, EmployeeSalaryComponent, EmployeeRealizationComponent],
  imports: [
    CommonModule,
    SharedModule,
    EmployeesRoutingModule,
    MatButtonModule,
    MatCheckboxModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatDividerModule,
    MatListModule,
    FuseSidebarModule,
    FuseSharedModule,
    NgxsModule.forFeature(),
    OverlayModule,
    MatTabsModule,
    MatDialogModule,
    TranslateModule.forChild(),
    FormlyModule.forChild()
  ],
  entryComponents: [EmployeeViewComponent, EmployeeAddComponent]
})
export class EmployeesModule {
}
