import {Component, Inject, OnInit} from '@angular/core';
import {IMPACT_OVERLAY_DATA, ImpactOverlayRef} from '@impactdk/ngx-overlay';
import {FormArray, FormBuilder, FormGroup} from '@angular/forms';
import {PositionService} from '../../../../core/service/position/position.service';
import {Observable, of, Subscription} from 'rxjs';
import {DatePipe} from '@angular/common';
import {ServiceService} from '../../../../core/service/service/service.service';
import {BookingService} from '../../../../core/service/booking.service';
import {take} from 'rxjs/operators';
import {ServiceSpecService} from '../../../../core/service/service-spec.service';

@Component({
  selector: 'app-add-booking',
  templateUrl: './add-booking.component.html',
  styleUrls: ['./add-booking.component.scss'],
  providers: [DatePipe]
})
export class AddBookingComponent implements OnInit {
  service$: Subscription;
  addUser = false;
  openSpecServices = false;
  customerSelected;
  positions$: Observable<any>;
  services$: Observable<any>;
  specServices$: Observable<any>;
  products$: Observable<any>;
  form: FormGroup;
  specServices: FormArray;
  procedures = 'regular';

  get formData() {
    return <FormArray> this.form.get('specServices');
  }

  constructor(
    public overlayRef: ImpactOverlayRef,
    @Inject(IMPACT_OVERLAY_DATA) public data: any,
    private fb: FormBuilder,
    private datePipe: DatePipe,
    private positionService: PositionService,
    private serviceService: ServiceService,
    private serviceSpecService: ServiceSpecService,
    private bookingService: BookingService
  ) {
    console.log(data);
    this.form = this.fb.group({
      firstname: null,
      lastname: null,
      phone: null,
      birthDay: null,
      employeeId: data.employee.id,
      userId: null,
      serviceId: null,
      status: 1,
      paymentType: 0,
      price: null,
      cash: null,
      card: null,
      minutes: null,
      balance: null,
      salePercent: null,
      salaryFixed: null,
      salaryPercent: data.employee.outputPercent,
      fixedPrice: null,
      bookingDate: data.date,
      startTime: data.time,
      endTime: null,
      isGift: null,
      isConsignation: null,
      description: null,
      specServices: this.fb.array([]),
      serviceProducts: this.fb.array([])
    });
  }


  ngOnInit() {
    this.form.get('serviceId').valueChanges.subscribe(item => {
      this.getServiceById(item);
    });
    this.getPositions();
    this.getServices();
    this.getSpecServices();
  }

  getServiceById(serviceId: any) {
    this.service$ = this.serviceService.getById(serviceId)
      .subscribe( item => {
      this.chooseService(item);
    })
  }

  getPositions() {
    this.positions$ = this.positionService.getPositions({});
  }

  getServices() {
    this.services$ = this.serviceService.userServices();
  }

  getSpecServices() {
    this.specServices$ = this.serviceService.special({});
  }

  chooseService(service) {
    if (service.serviceProducts && service.serviceProducts.length) {
      this.products$ = of(service.serviceProducts);
    } else {
      this.products$ = of(null);
    }

    this.form.patchValue({
      price: service.price,
      cash: service.price,
      serviceId: service.id,
      fixedPrice: service.fixSum,
      endTime: this.getTime(this.form.get('startTime'), service.time)
    });

    this.service$.unsubscribe();
  }

  transformDate(date) {
    return this.datePipe.transform(date, 'yyyy-MM-dd');
  }

  closeSidebar() {
    this.overlayRef.close();
  }

  chooseUser(event: any) {
    this.customerSelected = event;
    this.form.patchValue({userId: event.id});
  }

  addUserEvent($event: boolean) {
    this.addUser = $event;
  }

  submit() {
    if (this.form.valid) {
      const params = {
        ...this.form.value,
        bookingDate: new Date(this.form.value.bookingDate)
      };
      this.bookingService.create(params)
        .subscribe(res => {
          console.log(res);
          this.overlayRef.close(res);
        });
    }

  }

  hmsToSecondsOnly(str) {
    const p = str.split(':');
    let s = 0;
    let m = 1;

    while (p.length > 0) {
      s += m * parseInt(p.pop(), 10);
      m *= 60;
    }
    return s;
  }

  timeConvert(n) {
    const num = n;
    const hours = (num / 60);
    const rhours = Math.floor(hours) > 9 ? Math.floor(hours) : '0' + Math.floor(hours);
    const minutes = (hours - +rhours) * 60;
    const rminutes = Math.floor(minutes) > 9 ? Math.floor(minutes) : '0' + Math.floor(minutes);
    return `${rhours}:${rminutes}`;
  }

  private getTime(startTime, distance: any) {
    if (distance) {
      const start = this.hmsToSecondsOnly(startTime.value);
      const end = this.hmsToSecondsOnly(distance);
      return this.timeConvert(start + end);
    }
    return null;
  }

  chooseServiceById($event: any) {
    this.getServiceById($event);
  }
}
