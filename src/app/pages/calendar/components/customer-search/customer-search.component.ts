import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Observable, of} from 'rxjs';
import {FormBuilder, FormGroup} from '@angular/forms';
import {CustomerService} from '../../../../core/service/customer.service';

@Component({
  selector: 'app-customer-search',
  templateUrl: './customer-search.component.html',
  styleUrls: ['./customer-search.component.scss']
})
export class CustomerSearchComponent implements OnInit {
  @Output() selectChanged: EventEmitter<any> = new EventEmitter<any>();
  @Output() addUserEvent: EventEmitter<boolean> = new EventEmitter<boolean>();
  searchForm: FormGroup;
  search;
  selected;

  customers = [
    {
      firstname: 'Levan',
      lastname: 'Jmukhadze',
      phone: '555661277'
    },
    {
      firstname: 'Levan',
      lastname: 'Meskhishvili',
      phone: '555003232'
    }
  ];
  searchResult$: Observable<any>;

  constructor(
    private fb: FormBuilder,
    private customerService: CustomerService
  ) {}

  ngOnInit() {}

  searchEvent(event: string) {
    if (event && event.length > 1) {
      this.searchResult$ = this.customerService.search(event);
    } else {
      this.searchResult$ = of(null);
    }
  }

  selectItem(item: any) {
    this.selected = item;
    this.searchResult$ = of(null);
    this.selectChanged.emit(this.selected);
    this.search = null;
  }

  clearCustomer() {
    this.selected = null;
    this.addUserEvent.emit(false);
    this.search = null;
  }

  addUser() {
    this.addUserEvent.emit(true);
    this.search = null;
  }
}
