import {ChangeDetectionStrategy, Component, Input, OnDestroy, OnInit} from '@angular/core';
import {ControlContainer, FormArray, FormBuilder, FormGroup, FormGroupDirective} from '@angular/forms';
import {Observable, Subject} from 'rxjs';
import {EmployeeService} from '../../../../core/service/employee/employee.service';
import {takeUntil} from 'rxjs/operators';
import {ServiceService} from '../../../../core/service/service/service.service';

@Component({
  selector: 'app-spec-service-repeat-form',
  templateUrl: './spec-service-repeat-form.component.html',
  styleUrls: ['./spec-service-repeat-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  viewProviders: [
    {provide: ControlContainer, useExisting: FormGroupDirective}
  ]
})
export class SpecServiceRepeatFormComponent implements OnInit, OnDestroy {
  destroy$: Subject<boolean> = new Subject<boolean>();

  @Input() group: FormGroup;

  private _services = [];

  get services() {
    return this._services;
  }

  @Input()
  set services(value) {
    this._services = value;
    if (this._services && this._services.length > 0) {
      this._services.forEach(item => {
        this.formData.push(
          this.fb.group({
            serviceId: item.id,
            serviceName: item.name,
            service: item,
            employeeId: null,
            price: item.orice,
            salary: null
          })
        );
      });
    } else {
      this.formData.clear();
    }
  }


  specServices: FormArray;
  procedures = 'regular';
  employees$: Observable<any>;
  employees;

  get formData() {
    return <FormArray> this.group.get('specServices');
  }

  constructor(private fb: FormBuilder, private serviceService: ServiceService, private employeeService: EmployeeService) {
  }

  ngOnInit() {
    this.getEmployees();
    this.group.patchValue({specServices: this.fb.array([this.createItem()])});
  }

  getEmployees() {
    this.employeeService.getDropdown()
      .pipe(takeUntil(this.destroy$))
      .subscribe(res => {
        this.employees = res;
      });
  }

  createItem(): FormGroup {
    return this.fb.group({
      serviceId: null,
      employeeId: null,
      price: null,
      salary: null
    });
  }

  addItem(): void {
    this.formData.push(this.createItem());
  }

  removeItem(i) {

    this.formData.removeAt(this.formData.value.findIndex((value, index) => index === i));
  }

  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }

}
