import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {EmployeeService} from '../../../../core/service/employee/employee.service';
import {Observable} from 'rxjs';
import {OverlayService} from '@impactdk/ngx-overlay';
import {CategorySidebarComponent} from '../../../products/components/category-sidebar/category-sidebar.component';
import {AddBookingComponent} from '../add-booking/add-booking.component';
import {DatePipe} from '@angular/common';
import {CalendarService} from '../../../../core/service/calendar.service';
import {FormBuilder, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-daily',
  templateUrl: './daily.component.html',
  styleUrls: ['./daily.component.scss']
})
export class DailyComponent implements OnInit {
  dateform: FormGroup;


  splitTime = 15;
  employees$: Observable<any>;
  calendar$: Observable<any>;
  date =  new Date();

  intervalTimes = [
    {
      name: '00:15',
      value: 15
    },
    {
      name: '00:30',
      value: 30
    },
    {
      name: '01:00',
      value: 60
    }
  ];

  constructor(
    private employeeService: EmployeeService,
    private calendarService: CalendarService,
    private overlay: OverlayService,
    private cd: ChangeDetectorRef,
    private fb: FormBuilder
  ) {
    this.dateform = this.fb.group({
      date: new Date()
    });
    this.getCalendarBooking(this.date);
  }

  ngOnInit() {
    this.dateform.valueChanges.subscribe( input => {
      this.date = input.date;
      console.log(this.date);
      this.getCalendarBooking(this.date);
    })
    this.getEmployees();

  }

  getEmployees() {
    this.employees$ = this.employeeService.getDropdown();
  }

  getCalendarBooking(date) {
    const params = {
      date
    }
    this.calendar$ = this.calendarService.getCalendarBooking(params);
  }






  addBooking({time, employee}) {

    const overlayRef = this.overlay.open(AddBookingComponent, {
      data: {
        date: this.date,
        time,
        employee
      }
    });

    overlayRef
      .afterClose()
      .subscribe(response => {
        if (response) {
          console.log(response);
          this.getCalendarBooking(this.date);
        }
      });
  }

  changeInterval(interval: number) {
    this.splitTime = interval;
    this.cd.detectChanges();
  }
}
