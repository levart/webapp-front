import {ChangeDetectionStrategy, Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {AbstractControl, FormArray, FormBuilder, FormGroup} from '@angular/forms';
import {EmployeeService} from '../../../../core/service/employee/employee.service';
import {takeUntil} from 'rxjs/operators';

@Component({
  selector: 'app-service-product-reapeat-form',
  templateUrl: './service-product-reapeat-form.component.html',
  styleUrls: ['./service-product-reapeat-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ServiceProductReapeatFormComponent implements OnInit, OnDestroy {
  destroy$: Subject<boolean> = new Subject<boolean>();

  @Input() group: FormGroup;


  // tslint:disable-next-line:variable-name
  private _products = [];

  get products() {
    return this._products;
  }

  @Input()
  set products(value) {
    this._products = value;
    if (this._products && this._products.length > 0) {
      this._products.forEach(item => {
        this.formData.push(
          this.fb.group({
            product: item.product,
            productId: item.product.id,
            quantity: item.quantitySum,
            gram: item.grSum,
            price: item.product.salePricePerItem
          })
        );
      });
    } else {
      this.formData.clear();
    }
  }

  specServices: FormArray;
  procedures = 'regular';
  employees$: Observable<any>;
  employees;

  get formData() {
    return <FormArray> this.group.get('serviceProducts');
  }

  constructor(private fb: FormBuilder, private service: EmployeeService) {
  }

  ngOnInit() {
    console.log(this.products);
    this.group.patchValue({serviceProducts: this.fb.array([this.createItem()])});
  }

  getEmployees() {
    this.service.getDropdown()
      .pipe(takeUntil(this.destroy$))
      .subscribe(res => {
        this.employees = res;
      });
  }

  createItem(): FormGroup {
    return this.fb.group({
      productId: null,
      quantity: null,
      gram: null,
      price: null
    });
  }

  addItem(): void {
    this.formData.push(this.createItem());
  }

  removeItem(i) {
    console.log(i);
    this.formData.removeAt(this.formData.value.findIndex((value, index) => index === i));
  }

  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }

  chooseProduct($event: any) {
    console.log($event);
    this.formData.push(
      this.fb.group({
        product: $event,
        productId: $event.id,
        quantity: 0,
        gram: 0,
        price: $event.salePricePerItem
      })
    );
  }

  getSum(controls: AbstractControl[]) {
    console.log(controls);
    let sum = 0;
    controls.forEach(item => {
      console.log(item);
      sum += item.value.price;
    });
    this.group.patchValue({fixedPrice: sum});
    return sum;
  }
}
