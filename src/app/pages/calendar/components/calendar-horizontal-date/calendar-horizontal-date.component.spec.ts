import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalendarHorizontalDateComponent } from './calendar-horizontal-date.component';

describe('CalendarHorizontalDateComponent', () => {
  let component: CalendarHorizontalDateComponent;
  let fixture: ComponentFixture<CalendarHorizontalDateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalendarHorizontalDateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalendarHorizontalDateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
