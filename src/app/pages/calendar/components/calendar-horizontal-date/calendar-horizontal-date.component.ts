import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChange,
  SimpleChanges
} from '@angular/core';

@Component({
  selector: 'app-calendar-horizontal-date',
  templateUrl: './calendar-horizontal-date.component.html',
  styleUrls: ['./calendar-horizontal-date.component.scss']
})
export class CalendarHorizontalDateComponent implements OnInit, OnChanges {
  @Input() startHours = 6;
  @Input() endHours = 24;
  @Input() splitTime = 15;
  @Input() employees;

  @Output() clicked: EventEmitter<any> = new EventEmitter<any>();

  hourseDiff = 0;
  splitCount = 0;
  splitsArray = [];

  constructor() {
  }

  ngOnInit() {
    this.init(this.splitTime);
  }

  ngOnChanges(changes: SimpleChanges): void {
    const spltTime: SimpleChange = changes.splitTime;
    console.log(spltTime);
    if (spltTime) {
      this.splitTime = spltTime.currentValue;
    }
    this.init(this.splitTime);

  }

  init(splitTime) {
    this.hourseDiff = this.getHourseDiff();
    this.splitCount = this.getNodeCount(this.hourseDiff, splitTime);
    this.getSplits(this.splitCount, splitTime);

  }

  getHourseDiff(): number {
    return this.endHours - this.startHours;
  }

  getNodeCount(hourseDiff, splitTime): number {
    return hourseDiff * (60 / splitTime);
  }

  getSplits(splitCount, splitTime) {
    this.splitsArray = [];
    for (let i = 0; i < splitCount; i++) {
      this.splitsArray.push(i * splitTime);
    }

    console.log(this.splitsArray);
  }

  timeConvert(n) {
    const num = n;
    const hours = (num / 60) + this.startHours;
    const rhours = Math.floor(hours) > 9 ? Math.floor(hours) : '0' + Math.floor(hours);
    const minutes = (hours - +rhours) * 60;
    const rminutes = Math.floor(minutes) > 9 ? Math.floor(minutes) : '0' + Math.floor(minutes);
    return `${rhours}:${rminutes}`;
  }

  addBooking(split: any, employee: any) {
    this.clicked.emit({time: this.timeConvert(split), employee});
  }
}
