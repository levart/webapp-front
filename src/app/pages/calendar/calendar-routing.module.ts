import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {CalendarComponent} from './containers/calendar/calendar.component';
import {DailyComponent} from './components/daily/daily.component';
import {MonthlyComponent} from './components/monthly/monthly.component';


const routes: Routes = [
  {
    path: 'daily',
    component: DailyComponent
  },
  {
    path: 'month',
    component: MonthlyComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CalendarRoutingModule { }
