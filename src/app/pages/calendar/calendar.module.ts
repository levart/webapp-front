import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {CalendarRoutingModule} from './calendar-routing.module';
import {CalendarComponent} from './containers/calendar/calendar.component';
import {HourslyComponent} from './components/hoursly/hoursly.component';
import {DailyComponent} from './components/daily/daily.component';
import {MonthlyComponent} from './components/monthly/monthly.component';
import {SharedModule} from '../../shared/shared.module';
import {FlexLayoutModule} from '@angular/flex-layout';
import {AddBookingComponent} from './components/add-booking/add-booking.component';
import {OverlayModule} from '@impactdk/ngx-overlay';
import {CalendarHorizontalDateComponent} from './components/calendar-horizontal-date/calendar-horizontal-date.component';
import {CalendarGridHorizontalComponent} from './components/calendar-grid-horizontal/calendar-grid-horizontal.component';
import {CustomerSearchComponent} from './components/customer-search/customer-search.component';
import {ReactiveFormsModule} from '@angular/forms';
import {SpecServiceRepeatFormComponent} from './components/spec-service-repeat-form/spec-service-repeat-form.component';
import {ServiceProductReapeatFormComponent} from './components/service-product-reapeat-form/service-product-reapeat-form.component';
import {FuseSharedModule} from '../../../@fuse/shared.module';
import { TooltipModule } from 'ng2-tooltip-directive';

@NgModule({
  declarations: [
    CalendarComponent,
    HourslyComponent,
    DailyComponent,
    MonthlyComponent,
    AddBookingComponent,
    CalendarHorizontalDateComponent,
    CalendarGridHorizontalComponent,
    CustomerSearchComponent,
    SpecServiceRepeatFormComponent,
    ServiceProductReapeatFormComponent
  ],
  imports: [
    CommonModule,
    CalendarRoutingModule,
    SharedModule,
    FlexLayoutModule,
    OverlayModule,
    TooltipModule,
    FuseSharedModule
  ],
  exports: [],
  entryComponents: [
    AddBookingComponent
  ]
})
export class CalendarModule {
}
