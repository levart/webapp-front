import {Component, Inject, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {FormlyFieldConfig} from '@ngx-formly/core';
import {IMPACT_OVERLAY_DATA, ImpactOverlayRef} from '@impactdk/ngx-overlay';
import {ProductCategoryService} from '../../../../core/service/product-category/product-category.service';
import {ProductCategoryForm} from '../category/category-columns.data';

@Component({
  selector: 'app-category-sidebar',
  templateUrl: './category-sidebar.component.html',
  styleUrls: ['./category-sidebar.component.scss']
})
export class CategorySidebarComponent implements OnInit {

  form = new FormGroup({});
  model = {};
  fields: FormlyFieldConfig[] = ProductCategoryForm;


  constructor(
    public overlayRef: ImpactOverlayRef,
    @Inject(IMPACT_OVERLAY_DATA) public data: any,
    private service: ProductCategoryService,
  ) {
    if (this.data.isEdit) {
      this.model = {
        ...this.data.category,
      };
    }

  }

  ngOnInit() {
  }

  submit(model) {

    if (this.form.valid) {
      if (this.data.isEdit) {
        this.service.update(model)
          .subscribe(res => {
            this.overlayRef.close(true);
          });
      } else {
        this.service.create(model)
          .subscribe(res => {
            this.overlayRef.close(true);
          });
      }
    }
  }

  closeSidebar() {
    this.overlayRef.close();
  }

}
