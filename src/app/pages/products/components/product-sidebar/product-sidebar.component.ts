import {Component, Inject, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {FormlyFieldConfig} from '@ngx-formly/core';
import {IMPACT_OVERLAY_DATA, ImpactOverlayRef} from '@impactdk/ngx-overlay';
import {EmployeeService} from '../../../../core/service/employee/employee.service';
import {ProductForm} from '../products-list/products-columns.data';
import {ProductService} from '../../../../core/service/product/product.service';

@Component({
  selector: 'app-product-sidebar',
  templateUrl: './product-sidebar.component.html',
  styleUrls: ['./product-sidebar.component.scss']
})
export class ProductSidebarComponent implements OnInit {
  form = new FormGroup({});
  model = {};
  fields: FormlyFieldConfig[] = ProductForm;


  constructor(
    public overlayRef: ImpactOverlayRef,
    @Inject(IMPACT_OVERLAY_DATA) public data: any,
    private service: ProductService,
  ) {
    if (this.data.isEdit) {
      this.model = {
        distributorId: this.data.product.distributor.id,
        productCategoryId: this.data.product.productCategory.id,
        ...this.data.product,
      };
    }

  }

  ngOnInit() {
  }

  submit(model) {
    if (this.form.valid) {
      if (this.data.isEdit) {
        this.service.update(model)
          .subscribe(res => {
            this.overlayRef.close(true);
          });
      } else {
        this.service.create(model)
          .subscribe(res => {
            this.overlayRef.close(true);
          });
      }
    }
  }

  closeSidebar() {
    this.overlayRef.close();
  }



}
