import {Component, Inject, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {FormlyFieldConfig} from '@ngx-formly/core';
import {IMPACT_OVERLAY_DATA, ImpactOverlayRef} from '@impactdk/ngx-overlay';
import {DistributorService} from '../../../../core/service/distributor/distributor.service';
import {DistributorForm} from '../distributors/distributor-columns.data';

@Component({
  selector: 'app-distributor-sidebar',
  templateUrl: './distributor-sidebar.component.html',
  styleUrls: ['./distributor-sidebar.component.scss']
})
export class DistributorSidebarComponent implements OnInit {

  form = new FormGroup({});
  model = {};
  fields: FormlyFieldConfig[] = DistributorForm;


  constructor(
    public overlayRef: ImpactOverlayRef,
    @Inject(IMPACT_OVERLAY_DATA) public data: any,
    private service: DistributorService,
  ) {
    if (this.data.isEdit) {
      this.model = {
        ...this.data.distributor,
      };
    }

  }

  ngOnInit() {
  }

  submit(model) {
    if (this.form.valid) {
      if (this.data.isEdit) {
        this.service.update(model)
          .subscribe(res => {
            this.overlayRef.close(true);
          });
      } else {
        this.service.create(model)
          .subscribe(res => {
            this.overlayRef.close(true);
          });
      }
    }
  }

  closeSidebar() {
    this.overlayRef.close();
  }

}
