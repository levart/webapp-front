import {Column} from '../../../../core/model/column';
import {FormlyFieldConfig} from '@ngx-formly/core';


export const CategoryColumns: Column[] = [
  {
    columnName: 'name',
    columnTitle: 'COLUMNS.name',
    columnType: 'string'
  },
  {
    columnName: 'createdAt',
    columnTitle: 'COLUMNS.createdAt',
    columnType: 'datetime'
  }
];

export const ProductCategoryForm: FormlyFieldConfig[] = [
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          className: 'flex-1',
          key: 'name',
          type: 'wa-input',
          templateOptions: {
            label: 'დასახელება',
            required: true,
          }
        }
      ]
    }
  ];

