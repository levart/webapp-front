import { Component, OnInit } from '@angular/core';
import {Observable} from 'rxjs';
import {DistributorColumns} from '../distributors/distributor-columns.data';
import {FuseSidebarService} from '../../../../../@fuse/components/sidebar/sidebar.service';
import {DistributorService} from '../../../../core/service/distributor/distributor.service';
import {MatDialog} from '@angular/material';
import {ProductCategoryService} from '../../../../core/service/product-category/product-category.service';
import {PageParams} from '../../../../core/model/page-param';
import {CategoryColumns} from './category-columns.data';
import {ConfirmationDialogComponent} from '../../../../shared/components/confirmation-dialog/confirmation-dialog.component';
import {OverlayService} from '@impactdk/ngx-overlay';
import {ProductSidebarComponent} from '../product-sidebar/product-sidebar.component';
import {CategorySidebarComponent} from '../category-sidebar/category-sidebar.component';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit {

  isLoading = false;
  loadData$: Observable<any>;
  displayedColumns = CategoryColumns;
  paginatorPageSize = 10;
  paginatorPageIndex = 1;

  constructor(
    private fuseSidebarService: FuseSidebarService,
    private productCategoryService: ProductCategoryService,
    private overlay: OverlayService,
    public dialog: MatDialog
  ) { }

  ngOnInit() {
    this.getCategories();
  }

  private getCategories() {
    const params: PageParams = {
      page: this.paginatorPageIndex,
      pageSize: this.paginatorPageSize
    };
    this.loadData$ = this.productCategoryService.get(params);
  }

  paginatorChange(pageEvent) {
    this.paginatorPageSize = pageEvent.pageSize;
    this.paginatorPageIndex = pageEvent.page;
    this.getCategories();
  }

  delete(event) {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '600px',
      data: {
        message: 'ნამდვილად გსურთ წაშლა?',
        title: 'კატეგორიის წაშლა'
      }
    });
    return dialogRef.afterClosed()
      .toPromise()
      .then((result: boolean) => {
        if (result) {
          console.log(result);
          this.productCategoryService.delete(event)
            .pipe()
            .subscribe(res => {
              this.getCategories();
            });
        }
      });
  }

  edit(event, isEdit = false) {
    const overlayRef = this.overlay.open(CategorySidebarComponent, {
      data: {
        category: event,
        isEdit
      },
      fullHeight: true,
      positionVertical: {placement: 'bottom'},
      positionHorizontal: {placement: 'right'}
    });

    overlayRef
      .afterClose()
      .subscribe(response => {
        if (response) {
          this.getCategories();
        }
      });
  }
}
