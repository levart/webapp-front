import {Component, OnInit} from '@angular/core';
import {DistributorService} from '../../../../core/service/distributor/distributor.service';
import {Observable} from 'rxjs';
import {DistributorColumns} from './distributor-columns.data';
import {PageParams} from '../../../../core/model/page-param';
import {ConfirmationDialogComponent} from '../../../../shared/components/confirmation-dialog/confirmation-dialog.component';
import {MatDialog} from '@angular/material';
import {FuseSidebarService} from '../../../../../@fuse/components/sidebar/sidebar.service';
import {ProductSidebarComponent} from '../product-sidebar/product-sidebar.component';
import {OverlayService} from '@impactdk/ngx-overlay';
import {DistributorSidebarComponent} from '../distributor-sidebar/distributor-sidebar.component';

@Component({
  selector: 'app-distributors',
  templateUrl: './distributors.component.html',
  styleUrls: ['./distributors.component.scss']
})
export class DistributorsComponent implements OnInit {
  isLoading = false;
  loadData$: Observable<any>;
  displayedColumns = DistributorColumns;
  paginatorPageSize = 10;
  paginatorPageIndex = 1;

  constructor(
    private fuseSidebarService: FuseSidebarService,
    private distibutorService: DistributorService,
    private overlay: OverlayService,
    public dialog: MatDialog
  ) {
  }

  ngOnInit() {
    this.getDistributors();
  }

  getDistributors() {
    const params: PageParams = {
      page: this.paginatorPageIndex,
      pageSize: this.paginatorPageSize
    };
    this.loadData$ = this.distibutorService.get(params);
  }

  paginatorChange(pageEvent) {
    this.paginatorPageSize = pageEvent.pageSize;
    this.paginatorPageIndex = pageEvent.page;
    this.getDistributors();
  }

  openDialog(b: boolean, event: Event | string) {

  }

  delete(event: Event | string) {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '600px',
      data: {
        message: 'ნამდვილად გსურთ წაშლა?',
        title: 'მომწოდებლის წაშლა'
      }
    });
    return dialogRef.afterClosed()
      .toPromise()
      .then((result: boolean) => {
        if (result) {
          console.log(result);
          this.distibutorService.delete(event)
            .pipe()
            .subscribe(res => {
              console.log(res);
              this.getDistributors();
            });
        }
      });
  }


  edit(event, isEdit = false) {
    const overlayRef = this.overlay.open(DistributorSidebarComponent, {
      data: {
        distributor: event,
        isEdit
      },
      fullHeight: true,
      positionVertical: {placement: 'bottom'},
      positionHorizontal: {placement: 'right'}
    });

    overlayRef
      .afterClose()
      .subscribe(response => {
        if (response) {
          this.getDistributors();
        }
      });
  }

}
