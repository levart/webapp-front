import {Column} from '../../../../core/model/column';
import {FormlyFieldConfig} from '@ngx-formly/core';


export const DistributorColumns: Column[] = [
  {
    columnName: 'name',
    columnTitle: 'COLUMNS.name',
    columnType: 'string'
  },
  {
    columnName: 'legalForm',
    columnTitle: 'COLUMNS.legalForm',
    columnType: 'string'
  },
  {
    columnName: 'identityNumber',
    columnTitle: 'COLUMNS.identityNumber',
    columnType: 'string'
  },
  {
    columnName: 'phone',
    columnTitle: 'COLUMNS.phone',
    columnType: 'string'
  },
  {
    columnName: 'information',
    columnTitle: 'COLUMNS.information',
    columnType: 'string'
  },
  {
    columnName: 'createdAt',
    columnTitle: 'COLUMNS.createdAt',
    columnType: 'datetime'
  }
];


export const DistributorForm: FormlyFieldConfig[] = [
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          className: 'flex-1',
          key: 'name',
          type: 'wa-input',
          templateOptions: {
            label: 'დასახელება',
            required: true,
          }
        },
        {
          className: 'flex-1',
          key: 'legalForm',
          type: 'wa-input',
          templateOptions: {
            label: 'სამართლებრივი ფორმა',
            required: true,
          }
        }
      ],
    },
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          className: 'flex-1',
          key: 'identityNumber',
          type: 'wa-input',
          templateOptions: {
            label: 'საიდენტიფიკაცი #',
            required: true,
          }
        },
        {
          className: 'flex-1',
          key: 'phone',
          type: 'wa-input',
          templateOptions: {
            label: 'ტელეფონი'
          }
        },
      ],
    },
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          className: 'flex-1',
          key: 'information',
          type: 'textarea',
          templateOptions: {
            label: 'ინფორმაცია'
          }
        }
      ]
    }
  ]
;
