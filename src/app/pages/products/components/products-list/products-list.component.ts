import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {CategoryColumns} from '../category/category-columns.data';
import {ProductsColumns} from './products-columns.data';
import {FuseSidebarService} from '../../../../../@fuse/components/sidebar/sidebar.service';
import {MatDialog} from '@angular/material';
import {ProductService} from '../../../../core/service/product/product.service';
import {PageParams} from '../../../../core/model/page-param';
import {ConfirmationDialogComponent} from '../../../../shared/components/confirmation-dialog/confirmation-dialog.component';
import {map} from 'rxjs/operators';
import {EmployeeAddComponent} from '../../../employees/components/employee-add/employee-add.component';
import {OverlayService} from '@impactdk/ngx-overlay';
import {ProductSidebarComponent} from '../product-sidebar/product-sidebar.component';

@Component({
  selector: 'app-products-list',
  templateUrl: './products-list.component.html',
  styleUrls: ['./products-list.component.scss']
})
export class ProductsListComponent implements OnInit {
  isLoading = false;
  loadData$: Observable<any>;
  displayedColumns = ProductsColumns;
  paginatorPageSize = 10;
  paginatorPageIndex = 1;

  constructor(
    private fuseSidebarService: FuseSidebarService,
    private productService: ProductService,
    private overlay: OverlayService,
    public dialog: MatDialog
  ) {
  }

  ngOnInit() {
    this.getProducts();
  }

  private getProducts() {
    const params: PageParams = {
      page: this.paginatorPageIndex,
      pageSize: this.paginatorPageSize
    };
    this.loadData$ = this.productService.get(params);
  }

  paginatorChange(pageEvent) {
    this.paginatorPageSize = pageEvent.pageSize;
    this.paginatorPageIndex = pageEvent.page;
    this.getProducts();
  }

  delete(event) {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '600px',
      data: {
        message: 'ნამდვილად გსურთ წაშლა?',
        title: 'კატეგორიის წაშლა'
      }
    });
    return dialogRef.afterClosed()
      .toPromise()
      .then((result: boolean) => {
        if (result) {
          console.log(result);
          this.productService.delete(event)
            .pipe()
            .subscribe(res => {
              this.getProducts();
            });
        }
      });
  }

  edit(event, isEdit = false) {
    const overlayRef = this.overlay.open(ProductSidebarComponent, {
      data: {
        product: event,
        isEdit
      },
      fullHeight: true,
      positionVertical: {placement: 'bottom'},
      positionHorizontal: {placement: 'right'}
    });

    overlayRef
      .afterClose()
      .subscribe(response => {
        if (response) {
          this.getProducts();
        }
      });
  }
}
