import {Column} from '../../../../core/model/column';
import {FormlyFieldConfig} from '@ngx-formly/core';


export const ProductsColumns: Column[] = [
  {
    columnName: 'name',
    columnTitle: 'COLUMNS.name',
    columnType: 'string'
  },
  {
    columnName: 'productCategory',
    columnTitle: 'COLUMNS.productCategory',
    columnType: 'object',
    objectName: 'name',
    width: '250'
  },
  {
    columnName: 'distributor',
    columnTitle: 'COLUMNS.distributor',
    columnType: 'object',
    objectName: 'name'
  },
  {
    columnName: 'quantity',
    columnTitle: 'COLUMNS.quantity',
    columnType: 'string'
  },
  {
    columnName: 'grPerItem',
    columnTitle: 'COLUMNS.grPerItem',
    columnType: 'string'
  },
  {
    columnName: 'pricePerItem',
    columnTitle: 'COLUMNS.pricePerItem',
    columnType: 'string'
  },
  {
    columnName: 'salePricePerItem',
    columnTitle: 'COLUMNS.salePricePerItem',
    columnType: 'string'
  },
  {
    columnName: 'sum',
    columnTitle: 'COLUMNS.sum',
    columnType: 'string'
  },
  {
    columnName: 'grSum',
    columnTitle: 'COLUMNS.grSum',
    columnType: 'string'
  },
  {
    columnName: 'buyDate',
    columnTitle: 'COLUMNS.buyDate',
    columnType: 'date'
  },
  {
    columnName: 'createdAt',
    columnTitle: 'COLUMNS.createdAt',
    columnType: 'datetime'
  }
];


export const ProductForm: FormlyFieldConfig[] = [
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          className: 'flex-1',
          key: 'name',
          type: 'wa-input',
          templateOptions: {
            label: 'დასახელება',
            required: true,
          }
        }
      ],
    },
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          className: 'flex-1',
          key: 'productCategoryId',
          type: 'productCategories',
          templateOptions: {
            label: 'კატეგორია',
            required: true,
          }
        },
        {
          className: 'flex-1',
          key: 'distributorId',
          type: 'distributors',
          templateOptions: {
            label: 'მომწოდებელი'
          }
        },
      ],
    },
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          className: 'flex-1',
          key: 'quantity',
          type: 'wa-input',
          templateOptions: {
            label: 'რაოდენობა',
            required: true,
          }
        },
        {
          className: 'flex-1',
          key: 'grPerItem',
          type: 'wa-input',
          templateOptions: {
            label: 'გრამაჟი(ცალში)'
          }
        }
      ],
    },
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          className: 'flex-1',
          key: 'pricePerItem',
          type: 'wa-input',
          templateOptions: {
            label: 'მისაღები ფასი',
            required: true,
          }
        },
        {
          className: 'flex-1',
          key: 'salePricePerItem',
          type: 'wa-input',
          templateOptions: {
            label: 'გასაყიდი ფასი',
            required: true,
          }
        },
      ],
    },
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          className: 'flex-1',
          key: 'buyDate',
          type: 'wa-datepicker',
          templateOptions: {
            label: 'შეძენის თარიღი'
          }
        }
      ]
    }
  ]
;
