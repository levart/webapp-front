import { Component, OnInit } from '@angular/core';
import {FuseSidebarService} from '../../../../../@fuse/components/sidebar/sidebar.service';
import {DistributorService} from '../../../../core/service/distributor/distributor.service';
import {MatDialog} from '@angular/material';
import {ProductService} from '../../../../core/service/product/product.service';
import {Observable, Subject} from 'rxjs';
import {ProductCategoryService} from '../../../../core/service/product-category/product-category.service';
import {takeUntil} from 'rxjs/operators';
import {ActivatedRoute} from '@angular/router';
import {FormBuilder, FormGroup} from '@angular/forms';
import {OverlayService} from '@impactdk/ngx-overlay';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {
  private unsubscribe$ = new Subject<void>();

  addPositionState: any;
  addField: any;
  categories$: Observable<any>;
  private categoryId: any;
  form: FormGroup;

  constructor(
    private fuseSidebarService: FuseSidebarService,
    private productService: ProductService,
    private productCategoryService: ProductCategoryService,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    public dialog: MatDialog,
    private overlay: OverlayService,
  ) { }

  ngOnInit() {
    this.getCategories();
    this.route.queryParams
      .pipe(takeUntil(this.unsubscribe$)).subscribe(queryParams => {
      console.log(queryParams);
      if (queryParams.category) {
        this.categoryId = queryParams.category;
      }
    });
  }

  private getCategories() {
    this.categories$ = this.productCategoryService.get({});
  }

  toggleSidebar(name): void {
    this.fuseSidebarService.getSidebar(name).toggleOpen();
  }

  addDistributor() {

  }

  addCategory() {

  }

  submitCategory() {

  }

  editCategory(item: any) {

  }

  deleteCategory(id: any) {

  }


}
