import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {ProductsRoutingModule} from './products-routing.module';
import {ProductsComponent} from './container/products/products.component';
import {SharedModule} from '../../shared/shared.module';
import {FuseSidebarModule} from '../../../@fuse/components';
import {FuseSharedModule} from '../../../@fuse/shared.module';
import {NgxsModule} from '@ngxs/store';
import {OverlayModule} from '@impactdk/ngx-overlay';
import {TranslateModule} from '@ngx-translate/core';
import {CategoryComponent} from './components/category/category.component';
import {ProductsListComponent} from './components/products-list/products-list.component';
import {DistributorsComponent} from './components/distributors/distributors.component';
import { ProductSidebarComponent } from './components/product-sidebar/product-sidebar.component';
import { CategorySidebarComponent } from './components/category-sidebar/category-sidebar.component';
import { DistributorSidebarComponent } from './components/distributor-sidebar/distributor-sidebar.component';


@NgModule({
  declarations: [
    ProductsComponent,
    CategoryComponent,
    ProductsListComponent,
    DistributorsComponent,
    ProductSidebarComponent,
    CategorySidebarComponent,
    DistributorSidebarComponent
  ],
  imports: [
    CommonModule,
    ProductsRoutingModule,
    SharedModule,
    FuseSidebarModule,
    FuseSharedModule,
    NgxsModule.forFeature(),
    OverlayModule,
    TranslateModule.forChild()
  ],
  entryComponents: [
    ProductSidebarComponent,
    DistributorSidebarComponent,
    CategorySidebarComponent
  ]
})
export class ProductsModule {
}
