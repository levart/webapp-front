import {Component, OnDestroy, OnInit} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {DistributorColumns} from '../../../products/components/distributors/distributor-columns.data';
import {FuseSidebarService} from '../../../../../@fuse/components/sidebar/sidebar.service';
import {DistributorService} from '../../../../core/service/distributor/distributor.service';
import {MatDialog} from '@angular/material';
import {PageParams} from '../../../../core/model/page-param';
import {ConfirmationDialogComponent} from '../../../../shared/components/confirmation-dialog/confirmation-dialog.component';
import {TransactionService} from '../../../../core/service/transaction/transaction.service';
import {TransactionColumns} from '../../transaction-columns.data';
import {takeUntil} from 'rxjs/operators';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-transaction',
  templateUrl: './transaction.component.html',
  styleUrls: ['./transaction.component.scss']
})
export class TransactionComponent implements OnInit, OnDestroy {
  private unsubscribe$ = new Subject<void>();

  isLoading = false;
  loadData$: Observable<any>;
  displayedColumns = TransactionColumns;
  paginatorPageSize = 10;
  paginatorPageIndex = 1;

  isDeleted = false;

  constructor(
    private fuseSidebarService: FuseSidebarService,
    private transactionService: TransactionService,
    public dialog: MatDialog,
    private route: ActivatedRoute,
  ) {
  }

  ngOnInit() {

    this.route.params
      .pipe(takeUntil(this.unsubscribe$)).subscribe(params => {
      console.log(params);
      if (params.status && params.status === 'deleted') {
        this.isDeleted = true;
      } else {
        this.isDeleted = false;
      }
      console.log(this.isDeleted);
      this.getService(this.isDeleted);
    });
  }

  getService(isDeleted: boolean = false) {
    const params: PageParams = {
      page: this.paginatorPageIndex,
      pageSize: this.paginatorPageSize,
      isDeleted
    };
    this.loadData$ = this.transactionService.get(params);
  }

  paginatorChange(pageEvent) {
    this.paginatorPageSize = pageEvent.pageSize;
    this.paginatorPageIndex = pageEvent.page;
    this.getService();
  }

  openDialog(b: boolean, event: Event | string) {

  }

  delete(event: Event | string) {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '600px',
      data: {
        message: 'ნამდვილად გსურთ წაშლა?',
        title: 'მომწოდებლის წაშლა'
      }
    });
    return dialogRef.afterClosed()
      .toPromise()
      .then((result: boolean) => {
        if (result) {
          console.log(result);
          this.transactionService.delete(event)
            .pipe()
            .subscribe(res => {
              console.log(res);
              this.getService();
            });
        }
      });
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

}
