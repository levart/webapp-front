import {Column} from '../../core/model/column';


export const TransactionColumns: Column[] = [
  {
    columnName: 'id',
    columnTitle: '#',
    columnType: 'string'
  },
  {
    columnName: 'employee',
    columnTitle: 'COLUMNS.employee',
    columnType: 'object',
    objectName: 'firstname'
  },
  {
    columnName: 'serviceId',
    columnTitle: 'COLUMNS.service',
    columnType: 'string'
  },
  {
    columnName: 'price',
    columnTitle: 'COLUMNS.price',
    columnType: 'string'
  },
  {
    columnName: 'salePercent',
    columnTitle: 'COLUMNS.salePercent',
    columnType: 'string'
  },
  {
    columnName: 'fixedPrice',
    columnTitle: 'COLUMNS.fixedPrice',
    columnType: 'string'
  },
  {
    columnName: 'fixedSpend',
    columnTitle: 'COLUMNS.fixedSpend',
    columnType: 'string'
  },
  {
    columnName: 'productName',
    columnTitle: 'COLUMNS.productName',
    columnType: 'string'
  },
  {
    columnName: 'productVolume',
    columnTitle: 'COLUMNS.productVolume',
    columnType: 'string'
  },
  {
    columnName: 'quantity',
    columnTitle: 'COLUMNS.quantity',
    columnType: 'string'
  },
  {
    columnName: 'paymentType',
    columnTitle: 'COLUMNS.peymentType',
    columnType: 'string'
  },
  {
    columnName: 'createdBy',
    columnTitle: 'COLUMNS.createdBy',
    columnType: 'string'
  },
  {
    columnName: 'status',
    columnTitle: 'COLUMNS.status',
    columnType: 'string'
  },
  {
    columnName: 'transactionType',
    columnTitle: 'COLUMNS.transactionType',
    columnType: 'string'
  },
  {
    columnName: 'createdAt',
    columnTitle: 'COLUMNS.createdAt',
    columnType: 'datetime'
  }
];
