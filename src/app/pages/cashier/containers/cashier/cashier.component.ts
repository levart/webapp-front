import {Component, OnDestroy, OnInit} from '@angular/core';
import {Observable, Subscription} from 'rxjs';
import {CashierService} from '../../../../core/service/cashier.service';
import {PageParams} from '../../../../core/model/page-param';
import {OverlayService} from '@impactdk/ngx-overlay';

@Component({
  selector: 'app-cashier',
  templateUrl: './cashier.component.html',
  styleUrls: ['./cashier.component.scss']
})
export class CashierComponent implements OnInit, OnDestroy {
  sub$: Subscription;
  cashiers$: Observable<any>;
  paginatorPageSize = 10;
  paginatorPageIndex = 1;


  constructor(
    private cashierService: CashierService,
    private overlay: OverlayService,
  ) { }

  ngOnInit() {
    this.getCashier();
  }

  getCashier() {
    const params: PageParams = {
      page: this.paginatorPageIndex,
      pageSize: this.paginatorPageSize
    };
    this.cashiers$ = this.cashierService.get(params);
  }

  ngOnDestroy(): void {
  }

}
