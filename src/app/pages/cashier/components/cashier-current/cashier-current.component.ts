import { Component, OnInit } from '@angular/core';
import {CashierService} from '../../../../core/service/cashier.service';
import {OverlayService} from '@impactdk/ngx-overlay';
import {PageParams} from '../../../../core/model/page-param';
import {CashierModalComponent} from '../cashier-modal/cashier-modal.component';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-cashier-current',
  templateUrl: './cashier-current.component.html',
  styleUrls: ['./cashier-current.component.scss']
})
export class CashierCurrentComponent implements OnInit {
  openSum: number;
  cashiersCurrent$: Observable<any>;
  constructor(
    private cashierService: CashierService,
    private overlay: OverlayService,
  ) { }

  ngOnInit() {
    this.openSum = 5421;
    this.getCashier();
  }

  getCashier() {
    this.cashiersCurrent$ = this.cashierService.getCurrent();
  }


  cashierModal(openSum) {
    const overlayRef = this.overlay.open(CashierModalComponent, {
      data: {
        openSum,
      }
    });

    overlayRef
      .afterClose()
      .subscribe(response => {
        if (response) {
          console.log(response);
          this.getCashier();
        }
      });
  }

}
