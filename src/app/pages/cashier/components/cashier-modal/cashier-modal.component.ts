import {Component, Inject, OnInit} from '@angular/core';
import {IMPACT_OVERLAY_DATA, ImpactOverlayRef} from '@impactdk/ngx-overlay';
import {FormBuilder, FormGroup} from '@angular/forms';
import {CashierService} from '../../../../core/service/cashier.service';

@Component({
  selector: 'app-cashier-modal',
  templateUrl: './cashier-modal.component.html',
  styleUrls: ['./cashier-modal.component.scss']
})
export class CashierModalComponent implements OnInit {

  form: FormGroup;

  constructor(
    public overlayRef: ImpactOverlayRef,
    @Inject(IMPACT_OVERLAY_DATA) public data: any,
    private fb: FormBuilder,
    private cashierService: CashierService,
  ) {
    this.form = this.fb.group({
      openSum: data.openSum,
      comment: null
    });
  }

  ngOnInit() {
  }

  close() {
    this.overlayRef.close(false);
  }

  submit() {
    if (this.form.valid) {

      console.log(this.form.value);
      this.cashierService.openCashier(this.form.value)
        .subscribe( res => {
          if(res){
            this.overlayRef.close(true);
          }
        });
    }
  }
}
