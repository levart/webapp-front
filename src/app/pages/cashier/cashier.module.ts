import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CashierRoutingModule } from './cashier-routing.module';
import {CashierComponent} from './containers/cashier/cashier.component';
import {SharedModule} from '../../shared/shared.module';
import {FlexModule} from '@angular/flex-layout';
import { CashierModalComponent } from './components/cashier-modal/cashier-modal.component';
import {OverlayModule} from '@impactdk/ngx-overlay';
import {ReactiveFormsModule} from '@angular/forms';
import { CashierCurrentComponent } from './components/cashier-current/cashier-current.component';

@NgModule({
  declarations: [
    CashierComponent,
    CashierModalComponent,
    CashierCurrentComponent
  ],
  imports: [
    CommonModule,
    CashierRoutingModule,
    SharedModule,
    OverlayModule,
    FlexModule,
    ReactiveFormsModule
  ],
  exports: [
    CashierModalComponent
  ],
  entryComponents: [
    CashierModalComponent
  ]
})
export class CashierModule { }
