import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {CustomerComponent} from '../customer/components/customer/customer.component';
import {CashierComponent} from './containers/cashier/cashier.component';


const routes: Routes = [
  {
    path: '',
    component: CashierComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CashierRoutingModule { }
