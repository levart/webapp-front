import {Component, Inject, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {FormlyFieldConfig} from '@ngx-formly/core';
import {ServiceForm, ServiceSpecForm} from '../../service-columns.data';
import {IMPACT_OVERLAY_DATA, ImpactOverlayRef} from '@impactdk/ngx-overlay';
import {ServiceService} from '../../../../core/service/service/service.service';

@Component({
  selector: 'app-service-spec-sidebar',
  templateUrl: './service-spec-sidebar.component.html',
  styleUrls: ['./service-spec-sidebar.component.scss']
})
export class ServiceSpecSidebarComponent implements OnInit {

  form = new FormGroup({});
  model = {};
  fields: FormlyFieldConfig[] = ServiceSpecForm;


  constructor(
    public overlayRef: ImpactOverlayRef,
    @Inject(IMPACT_OVERLAY_DATA) public data: any,
    private service: ServiceService,
  ) {
    this.model = {
      ...this.model,
      isSpecial: this.data.isSpecial
    };
    if (this.data.isEdit) {
      this.model = {
        ...this.data.service,
        serviceCategoryId: this.data.service.serviceCategory.id
      };
    }
  }

  ngOnInit() {

  }

  submit(model) {
    if (this.form.valid) {
      if (this.data.isEdit) {
        this.service.update(model)
          .subscribe(res => {
            this.overlayRef.close(true);
          });
      } else {
        this.service.create(model)
          .subscribe(res => {
            this.overlayRef.close(true);
          });
      }
    }
  }

  closeSidebar() {
    this.overlayRef.close();
  }
}

