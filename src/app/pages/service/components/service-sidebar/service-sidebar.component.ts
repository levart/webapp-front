import {Component, Inject, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {FormlyFieldConfig} from '@ngx-formly/core';
import {IMPACT_OVERLAY_DATA, ImpactOverlayRef} from '@impactdk/ngx-overlay';
import {EmployeeService} from '../../../../core/service/employee/employee.service';
import {ServiceForm} from '../../service-columns.data';
import {ServiceService} from '../../../../core/service/service/service.service';

@Component({
  selector: 'app-service-sidebar',
  templateUrl: './service-sidebar.component.html',
  styleUrls: ['./service-sidebar.component.scss']
})
export class ServiceSidebarComponent implements OnInit {

  form = new FormGroup({});
  model = {};
  fields: FormlyFieldConfig[] = ServiceForm;


  constructor(
    public overlayRef: ImpactOverlayRef,
    @Inject(IMPACT_OVERLAY_DATA) public data: any,
    private service: ServiceService,
  ) {
    this.model = {
      ...this.model,
      investments: [{}],
      isSpecial: this.data.isSpecial
    };
    if (this.data.isEdit) {
      this.model = {
        ...this.data.service,
        serviceCategoryId: this.data.service.serviceCategory.id
      };
    }
  }

  ngOnInit() {

  }

  submit(model) {
    if (this.form.valid) {
      if (this.data.isEdit) {
        this.service.update(model)
          .subscribe(res => {
            this.overlayRef.close(true);
          });
      } else {
        this.service.create(model)
          .subscribe(res => {
            this.overlayRef.close(true);
          });
      }
    }
  }

  closeSidebar() {
    this.overlayRef.close();
  }
}

