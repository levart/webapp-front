import {Component, Inject, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {FormlyFieldConfig} from '@ngx-formly/core';
import {IMPACT_OVERLAY_DATA, ImpactOverlayRef} from '@impactdk/ngx-overlay';
import {EmployeeService} from '../../../../core/service/employee/employee.service';
import {ServiceForm} from '../../service-columns.data';
import {ServiceService} from '../../../../core/service/service/service.service';
import {ServiceCategoryForm} from '../../service-category-columns.data';
import {ServiceCategoryService} from '../../../../core/service/service-category/service-category.service';

@Component({
  selector: 'app-service-sidebar',
  templateUrl: './service-category-sidebar.component.html',
  styleUrls: ['./service-category-sidebar.component.scss']
})
export class ServiceCategorySidebarComponent implements OnInit {

  form = new FormGroup({});
  model = {};
  fields: FormlyFieldConfig[] = ServiceCategoryForm;


  constructor(
    public overlayRef: ImpactOverlayRef,
    @Inject(IMPACT_OVERLAY_DATA) public data: any,
    private service: ServiceCategoryService,
  ) {
    if (this.data.isEdit) {
      this.model = {
        ...this.data.service
      };
    }

  }

  ngOnInit() {
  }

  submit(model) {
    if (this.form.valid) {
      if (this.data.isEdit) {
        this.service.update(model.id, model)
          .subscribe(res => {
            this.overlayRef.close(true);
          });
      } else {

        this.service.create(model)
          .subscribe(res => {
            this.overlayRef.close(true);
          });
      }
    }
  }

  closeSidebar() {
    this.overlayRef.close();
  }
}

