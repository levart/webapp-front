import {Component, Inject, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {IMPACT_OVERLAY_DATA, ImpactOverlayRef} from '@impactdk/ngx-overlay';
import {EmployeeService} from '../../../../core/service/employee/employee.service';
import {ServiceService} from '../../../../core/service/service/service.service';
import {FormGroup} from '@angular/forms';
import {FormlyFieldConfig} from '@ngx-formly/core';
import {ServiceForm, ServiceSpecsAddForm} from '../../service-columns.data';
import {ServiceSpecService} from '../../../../core/service/service-spec.service';

@Component({
  selector: 'app-service-view',
  templateUrl: './service-view.component.html',
  styleUrls: ['./service-view.component.scss']
})
export class ServiceViewComponent implements OnInit {

  service$: Observable<any>;
  serviceSpec$: Observable<any>;
  form = new FormGroup({});
  model = {
    serviceId: null,
    serviceSpecs: []
  };
  fields: FormlyFieldConfig[] = ServiceSpecsAddForm;

  constructor(
    public overlayRef: ImpactOverlayRef,
    @Inject(IMPACT_OVERLAY_DATA) public data: string,
    private service: ServiceService,
    private serviceSpec: ServiceSpecService
  ) {
  }

  ngOnInit() {
    this.getEmployee(this.data);
    this.model = {
      ...this.model,
      serviceId: this.data
    };
  }

  getEmployee(employeeId) {
    this.service$ = this.service.getById(employeeId);
    this.serviceSpec$ = this.serviceSpec.getByService(employeeId);
    this.serviceSpec$.subscribe(item => {
      this.model = {
        ...this.model,
        serviceSpecs: item,
        serviceId: this.data
      };
    });
  }

  submit(model) {
    console.log(model);
    if (this.form.valid) {
      this.serviceSpec.create(model)
        .subscribe(res => {
          this.overlayRef.close(true);
        });
    }
  }

  closeSidebar() {
    this.overlayRef.close();
  }
}
