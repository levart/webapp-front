import {Component, Input, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {FormlyFieldConfig} from '@ngx-formly/core';
import {ServiceProductsAddForm} from '../../service-columns.data';
import {ServiceProductsService} from '../../../../core/service/service-products.service';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-service-products',
  templateUrl: './service-products.component.html',
  styleUrls: ['./service-products.component.scss']
})
export class ServiceProductsComponent implements OnInit {

  @Input() service;
  form = new FormGroup({});
  model = {
    serviceId: null,
    autoFix: false,
    fixSum: null,
    serviceProducts: []
  };

  fields: FormlyFieldConfig[] = ServiceProductsAddForm;
  serviceProducts$: Observable<any>;

  constructor(
    private serviceProducts: ServiceProductsService
  ) {
  }

  ngOnInit() {
    this.model = {
      ...this.model,
      serviceId: this.service.id,
      autoFix: this.service.autoFix,
      fixSum: this.service.fixSum,
    };
    this.getServiceproducts(this.service.id);
  }

  getServiceproducts(serviceId) {
    this.serviceProducts$ = this.serviceProducts.getByService(serviceId);
    this.serviceProducts$.subscribe(item => {
      this.model = {
        ...this.model,
        serviceProducts: item
      };
    });
  }

  submit(model) {
    if (this.form.valid) {
      this.serviceProducts.create(model)
        .subscribe(res => {
          console.log(res);
        });
    }
  }
}
