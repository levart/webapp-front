import {ChangeDetectionStrategy, Component, Input, OnChanges, OnInit, SimpleChange, SimpleChanges} from '@angular/core';
import {Observable} from 'rxjs';
import {FuseSidebarService} from '../../../../../@fuse/components/sidebar/sidebar.service';
import {OverlayService} from '@impactdk/ngx-overlay';
import {MatDialog} from '@angular/material';
import {ConfirmationDialogComponent} from '../../../../shared/components/confirmation-dialog/confirmation-dialog.component';
import {ServiceService} from '../../../../core/service/service/service.service';
import {ServiceColumns} from '../../service-columns.data';
import {ServiceSidebarComponent} from '../../components/service-sidebar/service-sidebar.component';
import {ServiceViewComponent} from '../../components/service-view/service-view.component';
import {ServiceSpecSidebarComponent} from '../../components/service-spec-sidebar/service-spec-sidebar.component';

@Component({
  selector: 'app-service-content',
  templateUrl: './service-content.component.html',
  styleUrls: ['./service-content.component.scss']
})
export class ServiceContentComponent implements OnInit, OnChanges {

  @Input() categoryId: string;
  @Input() isSpecial: any;
  isLoading = false;

  displayedColumns = ServiceColumns;
  paginatorPageSize = 10;
  paginatorPageIndex = 1;
  employees$: Observable<any>;

  constructor(
    private fuseSidebarService: FuseSidebarService,
    private serviceService: ServiceService,
    private overlay: OverlayService,
    public dialog: MatDialog,
  ) {
  }

  ngOnInit() {

    this.getEmployees(this.categoryId, this.isSpecial);
  }

  ngOnChanges(changes: SimpleChanges): void {
    const categoryId: SimpleChange = changes.categoryId.currentValue;
    const isSpecial = changes.isSpecial.currentValue;
    if (categoryId || isSpecial) {
      this.getEmployees(categoryId, isSpecial);
    }
  }

  getEmployees(serviceCategoryId: any, isSpecial = false) {
    const params = {
      page: this.paginatorPageIndex,
      pageSize: this.paginatorPageSize,
      isSpecial
    };
    this.employees$ = this.serviceService.getEmployees(params);
  }

  paginatorChange(pageEvent) {
    this.paginatorPageSize = pageEvent.pageSize;
    this.paginatorPageIndex = pageEvent.page;
    this.getEmployees(this.categoryId, this.isSpecial);
  }

  delete(event) {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '600px',
      data: {
        message: 'ნამდვილად გსურთ წაშლა?',
        title: 'თანამშრომლის წაშლა'
      }
    });
    return dialogRef.afterClosed()
      .toPromise()
      .then((result: boolean) => {
        if (result) {
          console.log(result);
          this.serviceService.deleteEmployee(event)
            .pipe()
            .subscribe(res => {
              console.log(res);
              this.getEmployees(this.categoryId, this.isSpecial);
            });
        }
      });
  }

  view(event) {
    const overlayRef = this.overlay.open(ServiceViewComponent, {
      data: event.id,
      fullHeight: true,
      positionVertical: {placement: 'bottom'},
      positionHorizontal: {placement: 'right'}
    });

    overlayRef
      .afterClose()
      .subscribe(response => {
        console.log(response);
        if (response) {

        }
      });
  }

  add(event, isEdit = false) {
    let overlayRef;
    if (this.isSpecial) {
      overlayRef = this.overlay.open(ServiceSpecSidebarComponent, {
        data: {
          service: event,
          isEdit,
          isSpecial: this.isSpecial
        },
        fullHeight: true,
        positionVertical: {placement: 'bottom'},
        positionHorizontal: {placement: 'right'}
      });
    } else {
      overlayRef = this.overlay.open(ServiceSidebarComponent, {
        data: {
          service: event,
          isEdit,
          isSpecial: this.isSpecial
        },
        fullHeight: true,
        positionVertical: {placement: 'bottom'},
        positionHorizontal: {placement: 'right'}
      });
    }

    overlayRef
      .afterClose()
      .subscribe(response => {
        if (response) {
          this.getEmployees(this.categoryId, this.isSpecial);
        }
      });
  }

  toggleSidebar(name): void {
    this.fuseSidebarService.getSidebar(name).toggleOpen();
  }

}
