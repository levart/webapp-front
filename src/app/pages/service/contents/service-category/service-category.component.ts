import {Component, Input, OnChanges, OnInit, SimpleChange, SimpleChanges} from '@angular/core';
import {ServiceColumns} from '../../service-columns.data';
import {Observable} from 'rxjs';
import {FuseSidebarService} from '../../../../../@fuse/components/sidebar/sidebar.service';
import {ServiceService} from '../../../../core/service/service/service.service';
import {OverlayService} from '@impactdk/ngx-overlay';
import {MatDialog} from '@angular/material';
import {map} from 'rxjs/operators';
import {ConfirmationDialogComponent} from '../../../../shared/components/confirmation-dialog/confirmation-dialog.component';
import {ServiceViewComponent} from '../../components/service-view/service-view.component';
import {ServiceSidebarComponent} from '../../components/service-sidebar/service-sidebar.component';
import {ServiceCategorySidebarComponent} from '../../components/service-category-sidebar/service-category-sidebar.component';
import {ServiceCategoryService} from '../../../../core/service/service-category/service-category.service';
import {ServiceCategoryColumns} from '../../service-category-columns.data';

@Component({
  selector: 'app-service-category',
  templateUrl: './service-category.component.html',
  styleUrls: ['./service-category.component.scss']
})
export class ServiceCategoryComponent implements OnInit, OnChanges {

  @Input() categoryId: string;
  isLoading = false;

  displayedColumns = ServiceCategoryColumns;
  paginatorPageSize = 10;
  paginatorPageIndex = 1;
  employees$: Observable<any>;

  constructor(
    private fuseSidebarService: FuseSidebarService,
    private serviceCategoryService: ServiceCategoryService,
    private overlay: OverlayService,
    public dialog: MatDialog,
  ) {
  }

  ngOnInit() {

    this.getEmployees(this.categoryId);
  }

  ngOnChanges(changes: SimpleChanges): void {
    const categoryId: SimpleChange = changes.categoryId.currentValue;
    if (categoryId) {
      this.getEmployees(categoryId);
    }
  }

  getEmployees(serviceCategoryId: any) {
    const params = {
      page: this.paginatorPageIndex,
      pageSize: this.paginatorPageSize,
      serviceCategoryId
    };
    this.employees$ = this.serviceCategoryService.getPagination(params).pipe(
      map(item => {
        return {
          ...item,
          data: item.data.map(m => {
            return {
              ...m.user,
              ...m,
            };
          }),

        };
      })
    );
  }

  paginatorChange(pageEvent) {
    this.paginatorPageSize = pageEvent.pageSize;
    this.paginatorPageIndex = pageEvent.page;
    this.getEmployees(this.categoryId);
  }

  delete(event) {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '600px',
      data: {
        message: 'ნამდვილად გსურთ წაშლა?',
        title: 'თანამშრომლის წაშლა'
      }
    });
    return dialogRef.afterClosed()
      .toPromise()
      .then((result: boolean) => {
        if (result) {
          console.log(result);
          this.serviceCategoryService.delete(event)
            .pipe()
            .subscribe(res => {
              console.log(res);
              this.getEmployees(this.categoryId);
            });
        }
      });
  }

  view(event) {
    console.log(event);
    const overlayRef = this.overlay.open(ServiceViewComponent, {
      data: event.id,
      fullHeight: true,
      positionVertical: {placement: 'bottom'},
      positionHorizontal: {placement: 'right'}
    });

    overlayRef
      .afterClose()
      .subscribe(response => {
        console.log(response);
        if (response) {

        }
      });
  }

  add(event, isEdit = false) {
    const overlayRef = this.overlay.open(ServiceCategorySidebarComponent, {
      data: {
        service: event,
        isEdit
      },
      fullHeight: true,
      positionVertical: {placement: 'bottom'},
      positionHorizontal: {placement: 'right'}
    });

    overlayRef
      .afterClose()
      .subscribe(response => {
        if (response) {
          this.getEmployees(this.categoryId);
        }
      });
  }

  toggleSidebar(name): void {
    this.fuseSidebarService.getSidebar(name).toggleOpen();
  }

}
