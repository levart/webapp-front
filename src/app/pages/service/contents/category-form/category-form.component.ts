import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-category-form',
  templateUrl: './category-form.component.html',
  styleUrls: ['./category-form.component.scss']
})
export class CategoryFormComponent implements OnInit {

  @Input() data: any;
  @Output() dataChange: EventEmitter<any> = new EventEmitter<any>();

  form: FormGroup;

  constructor(private formBuilder: FormBuilder) {
    this.form = this.formBuilder.group({
      id: [null],
      name: [null, Validators.required]
    });
  }

  ngOnInit() {
    this.form = this.formBuilder.group({
      id: [this.data.id],
      name: [this.data.name, Validators.required]
    });
  }

  submitUpdatePosition() {
    if (this.form.valid) {
      this.dataChange.emit(this.form.value);
    }
  }
}
