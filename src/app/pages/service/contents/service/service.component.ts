import {Component, OnDestroy, OnInit} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {FuseSidebarService} from '../../../../../@fuse/components/sidebar/sidebar.service';
import {PositionService} from '../../../../core/service/position/position.service';
import {ActivatedRoute} from '@angular/router';
import {MatDialog} from '@angular/material';
import {OverlayService} from '@impactdk/ngx-overlay';
import {map, takeUntil} from 'rxjs/operators';
import {ConfirmationDialogComponent} from '../../../../shared/components/confirmation-dialog/confirmation-dialog.component';
import {ServiceCategoryService} from '../../../../core/service/service-category/service-category.service';
import {ProductService} from '../../../../core/service/product/product.service';
import {ProductCategoryService} from '../../../../core/service/product-category/product-category.service';

@Component({
  selector: 'app-service',
  templateUrl: './service.component.html',
  styleUrls: ['./service.component.scss']
})
export class ServiceComponent implements OnInit {
  private unsubscribe$ = new Subject<void>();

  addPositionState: any;
  addField: any;
  categories$: Observable<any>;
  private categoryId: any;
  form: FormGroup;

  constructor(
    private fuseSidebarService: FuseSidebarService,
    private productService: ProductService,
    private productCategoryService: ProductCategoryService,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    public dialog: MatDialog,
    private overlay: OverlayService,
  ) { }

  ngOnInit() {
    this.getCategories();
    this.route.queryParams
      .pipe(takeUntil(this.unsubscribe$)).subscribe(queryParams => {
      console.log(queryParams);
      if (queryParams.category) {
        this.categoryId = queryParams.category;
      }
    });
  }

  private getCategories() {
    this.categories$ = this.productCategoryService.get({});
  }

  toggleSidebar(name): void {
    this.fuseSidebarService.getSidebar(name).toggleOpen();
  }

  addDistributor() {

  }

  addCategory() {

  }

  submitCategory() {

  }

  editCategory(item: any) {

  }

  deleteCategory(id: any) {

  }


}
