import {Column} from '../../core/model/column';
import {FormlyFieldConfig} from '@ngx-formly/core';


export const ServiceColumns: Column[] = [
  {
    columnName: 'name',
    columnTitle: 'COLUMNS.name',
    columnType: 'string'
  },
  {
    columnName: 'price',
    columnTitle: 'COLUMNS.price',
    columnType: 'string'
  },
  {
    columnName: 'time',
    columnTitle: 'COLUMNS.time',
    columnType: 'string'
  },
  {
    columnName: 'autoPriceView',
    columnTitle: 'COLUMNS.autoPriceView',
    columnType: 'booleanDot'
  },
  {
    columnName: 'autoTimeView',
    columnTitle: 'COLUMNS.autoTimeView',
    columnType: 'booleanDot'
  },
  {
    columnName: 'serviceCategory',
    columnTitle: 'COLUMNS.category',
    columnType: 'objectColored',
    objectName: 'name'
  },
  {
    columnName: 'createdAt',
    columnTitle: 'COLUMNS.createdAt',
    columnType: 'datetime'
  }
];


export const ServiceForm: FormlyFieldConfig[] = [
  {
    fieldGroupClassName: 'display-flex',
    fieldGroup: [
      {
        className: 'flex-1',
        key: 'name',
        type: 'wa-input',
        templateOptions: {
          label: 'სახელი',
          required: true,
        }
      },
      {
        className: 'flex-1',
        key: 'price',
        type: 'wa-input',
        templateOptions: {
          label: 'ღირებულება',
          required: true,
        }
      },
    ],
  },
  {
    fieldGroupClassName: 'display-flex',
    fieldGroup: [
      {
        className: 'flex-1',
        key: 'time',
        type: 'timepiker',
        templateOptions: {
          label: 'დრო',
          required: true,
        }
      },
      {
        className: 'flex-1',
        key: 'serviceCategoryId',
        type: 'serviceCategories',
        templateOptions: {
          label: 'კატეგორია'
        }
      },
    ],
  },

  {
    fieldGroupClassName: 'display-flex',
    fieldGroup: [
      {
        className: 'flex-1',
        key: 'autoPriceView',
        type: 'toggle',
        defaultValue: true,
        templateOptions: {
          label: 'ფასი ავტომატურად',
        }
      },
      {
        className: 'flex-1',
        key: 'autoTimeView',
        type: 'toggle',
        defaultValue: true,
        templateOptions: {
          label: 'დრო ავტომატურად'
        }
      },
    ],
  }
];

export const ServiceSpecsAddForm: FormlyFieldConfig[] = [

  {
    key: 'serviceSpecs',
    type: 'repeat',
    templateOptions: {
      addText: 'სპეციალური პროცედურის დამატება',
      label: 'სპეციალური პროცედურა'
    },
    fieldArray: {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [

        {
          type: 'services',
          key: 'specServiceId',
          className: 'flex-1',
          templateOptions: {
            label: 'პროცედურა',
          },
        },
        {
          className: 'flex-1',
          type: 'employees',
          key: 'employeeId',
          templateOptions: {
            label: 'თანამშრომელი',
            required: true,
          },
        },
        {
          type: 'wa-input',
          key: 'price',
          className: 'flex-1',
          templateOptions: {
            type: 'number',
            label: 'ფასი'
          },
        },
        {
          type: 'wa-input',
          key: 'salary',
          className: 'flex-1',
          templateOptions: {
            type: 'number',
            label: 'ხელფასი'
          },
        },
      ],
    },
  }
];

export const ServiceProductsAddForm: FormlyFieldConfig[] = [
  {
    key: 'serviceProducts',
    type: 'repeat',
    templateOptions: {
      addText: 'პროდუქტის დამატება',
      label: 'პროდუქტები'
    },
    fieldArray: {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          type: 'products',
          key: 'productId',
          className: 'flex-1',
          templateOptions: {
            label: 'პროდუქტი',
          },
        },
        {
          type: 'wa-input',
          key: 'grSum',
          className: 'flex-1',
          templateOptions: {
            type: 'number',
            label: 'გრამი'
          },
        },
        {
          type: 'wa-input',
          key: 'quantitySum',
          className: 'flex-1',
          templateOptions: {
            type: 'number',
            label: 'ცალი'
          },
        },
      ],
    },
  },
  {
    fieldGroupClassName: 'display-flex',
    fieldGroup: [
      {
        type: 'toggle',
        key: 'autoFix',
        className: 'flex-1',
        templateOptions: {
          type: 'number',
          label: 'ავტ.გადატანა ფიქს. ხარჯში'
        },
      },
      {
        type: 'wa-input',
        key: 'fixSum',
        className: 'flex-1',
        templateOptions: {
          type: 'number',
          label: 'ფასი'
        },
      }
    ]
  }

];


export const ServiceSpecForm: FormlyFieldConfig[] = [
  {
    fieldGroupClassName: 'display-flex',
    fieldGroup: [
      {
        className: 'flex-1',
        key: 'name',
        type: 'wa-input',
        templateOptions: {
          label: 'სახელი',
          required: true,
        }
      },
      {
        className: 'flex-1',
        key: 'price',
        type: 'wa-input',
        templateOptions: {
          label: 'ღირებულება',
          required: true,
        }
      },
    ],
  },
  {
    fieldGroupClassName: 'display-flex',
    fieldGroup: [
      {
        className: 'flex-1',
        key: 'time',
        type: 'wa-input',
        templateOptions: {
          label: 'დრო',
          required: true,
        }
      },
      {
        className: 'flex-1',
        key: 'serviceCategoryId',
        type: 'serviceCategories',
        templateOptions: {
          label: 'კატეგორია'
        }
      },
    ],
  },

  {
    fieldGroupClassName: 'display-flex',
    fieldGroup: [
      {
        className: 'flex-1',
        key: 'autoPriceView',
        type: 'toggle',
        defaultValue: true,
        templateOptions: {
          label: 'ფასი ავტომატურად',
        }
      },
      {
        className: 'flex-1',
        key: 'autoTimeView',
        type: 'toggle',
        defaultValue: true,
        templateOptions: {
          label: 'დრო ავტომატურად'
        }
      },
    ],
  }
];
