import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {ServiceRoutingModule} from './service-routing.module';
import {ServiceComponent} from './contents/service/service.component';
import {SharedModule} from 'app/shared/shared.module';
import {ServiceContentComponent} from './contents/service-content/service-content.component';
import {CategoryFormComponent} from './contents/category-form/category-form.component';
import {FuseSidebarModule} from '../../../@fuse/components';
import {FuseSharedModule} from '../../../@fuse/shared.module';
import {NgxsModule} from '@ngxs/store';
import {OverlayModule} from '@impactdk/ngx-overlay';
import {MatDialogModule, MatTabsModule} from '@angular/material';
import {TranslateModule} from '@ngx-translate/core';
import {FormlyModule} from '@ngx-formly/core';
import {ServiceSidebarComponent} from './components/service-sidebar/service-sidebar.component';
import {ServiceViewComponent} from './components/service-view/service-view.component';
import {ServiceCategoryComponent} from './contents/service-category/service-category.component';
import {ServiceCategorySidebarComponent} from './components/service-category-sidebar/service-category-sidebar.component';
import {ServiceSpecSidebarComponent} from './components/service-spec-sidebar/service-spec-sidebar.component';
import {ServiceProductsComponent} from './components/service-products/service-products.component';

@NgModule({
  declarations: [ServiceComponent, ServiceContentComponent, CategoryFormComponent, ServiceSidebarComponent, ServiceViewComponent, ServiceCategoryComponent, ServiceCategorySidebarComponent, ServiceSpecSidebarComponent, ServiceProductsComponent],
  imports: [
    CommonModule,
    ServiceRoutingModule,
    SharedModule,
    FuseSidebarModule,
    FuseSharedModule,
    NgxsModule.forFeature(),
    OverlayModule,
    MatTabsModule,
    MatDialogModule,
    TranslateModule.forChild(),
    FormlyModule.forChild()
  ],
  entryComponents: [ServiceSidebarComponent, ServiceViewComponent, ServiceCategorySidebarComponent, ServiceSpecSidebarComponent]
})
export class ServiceModule {
}
