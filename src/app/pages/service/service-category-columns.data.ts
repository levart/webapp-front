import {Column} from '../../core/model/column';
import {FormlyFieldConfig} from '@ngx-formly/core';


export const ServiceCategoryColumns: Column[] = [
  {
    columnName: 'name',
    columnTitle: 'COLUMNS.name',
    columnType: 'string'
  },
  {
    columnName: 'color',
    columnTitle: 'COLUMNS.color',
    columnType: 'color'
  },
  {
    columnName: 'position',
    columnTitle: 'COLUMNS.position',
    columnType: 'object',
    objectName: 'name'
  },
  {
    columnName: 'createdAt',
    columnTitle: 'COLUMNS.createdAt',
    columnType: 'datetime'
  }
];


export const ServiceCategoryForm: FormlyFieldConfig[] = [
  {
    fieldGroupClassName: 'display-flex',
    fieldGroup: [
      {
        className: 'flex-1',
        key: 'name',
        type: 'wa-input',
        templateOptions: {
          label: 'სახელი',
          required: true,
        }
      }
    ],
  },
  {
    fieldGroupClassName: 'display-flex',
    fieldGroup: [
      {
        className: 'flex-1',
        key: 'color',
        type: 'colors',
        defaultValue: '#000',
        templateOptions: {
          label: 'ფერი'
        }
      },
      {
        className: 'flex-1',
        key: 'positionId',
        type: 'positions',
        templateOptions: {
          label: 'პოზიცია'
        }
      },
    ],
  }
];
