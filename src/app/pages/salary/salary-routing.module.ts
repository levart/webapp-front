import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {SalaryComponent} from './container/salary/salary.component';


const routes: Routes = [{
  path: '',
  component: SalaryComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SalaryRoutingModule { }
