import {Component, Inject, OnInit} from '@angular/core';
import {IMPACT_OVERLAY_DATA, ImpactOverlayRef} from '@impactdk/ngx-overlay';

@Component({
  selector: 'app-salary-preview',
  templateUrl: './salary-preview.component.html',
  styleUrls: ['./salary-preview.component.scss']
})
export class SalaryPreviewComponent implements OnInit {

  constructor(
    public overlayRef: ImpactOverlayRef,
    @Inject(IMPACT_OVERLAY_DATA) public data: any,
  ) { }

  ngOnInit() {
  }

  close() {
    this.overlayRef.close();
  }
}
