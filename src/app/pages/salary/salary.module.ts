import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SalaryRoutingModule } from './salary-routing.module';
import { SalaryComponent } from './container/salary/salary.component';
import {FlexLayoutModule, FlexModule} from '@angular/flex-layout';
import {SharedModule} from '../../shared/shared.module';
import {FormsModule} from '@angular/forms';
import { SalaryPreviewComponent } from './components/salary-preview/salary-preview.component';
import {OverlayModule} from '@impactdk/ngx-overlay';


@NgModule({
  declarations: [SalaryComponent, SalaryPreviewComponent],
  imports: [
    CommonModule,
    SalaryRoutingModule,
    OverlayModule,
    SharedModule,
    FormsModule,
    FlexLayoutModule
  ],
  exports: [SalaryPreviewComponent],
  entryComponents: [SalaryPreviewComponent]
})
export class SalaryModule { }
