import {Component, OnDestroy, OnInit} from '@angular/core';
import {SelectionModel} from '@angular/cdk/collections';
import {Observable, of, Subscription} from 'rxjs';
import {EmployeeService} from '../../../../core/service/employee/employee.service';
import {map, mergeMap} from 'rxjs/operators';
import {OverlayService} from '@impactdk/ngx-overlay';
import {EmployeeAddComponent} from '../../../employees/components/employee-add/employee-add.component';
import {SalaryPreviewComponent} from '../../components/salary-preview/salary-preview.component';

@Component({
  selector: 'app-sallary',
  templateUrl: './salary.component.html',
  styleUrls: ['./salary.component.scss']
})
export class SalaryComponent implements OnInit, OnDestroy {
  sub$: Subscription;
  public salary: any = {};
  employees$: Observable<any>;
  employees;
  salarySum$: Observable<number> = of(0);


  selectedRow: any;
  selection = new SelectionModel<any>(true, []);

  constructor(
    private employeeService: EmployeeService,
    private overlay: OverlayService,
  ) {
    this.selectedRow = [];
  }

  ngOnInit() {
    this.getEmployees();
  }

  getEmployees() {
    this.sub$ = this.employeeService.getDropdown()
      .subscribe((result) => {
        this.employees = result;
      });
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.employees.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.employees.forEach(row => this.selection.select(row));
  }

  onChange(event): void {
    const salaryArr = Object.values(this.salary);
    const numOr0 = n => isNaN(n) ? 0 : n;
    let sum = 0;
    salaryArr.forEach(item => {
      sum += +item;
    });
    this.salarySum$ = of(sum);
  }


  previewSalaries() {
    const overlayRef = this.overlay.open(SalaryPreviewComponent, {
      data: {
        salary: Object.entries(this.salary),
      }
    });

    overlayRef
      .afterClose()
      .subscribe(response => {
        if (response) {
          console.log(response);
        }
      });
  }

  ngOnDestroy(): void {
    this.sub$.unsubscribe();
  }

}
