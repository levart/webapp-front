import { Navigate } from '@ngxs/router-plugin';
import { Action, Selector, State, StateContext } from '@ngxs/store';
import { IAuth } from 'app/core/model/auth';
import { AuthService } from 'app/core/service/auth/auth.service';
import { tap } from 'rxjs/operators';
import { Auth, Login, LoginRedirect, Logout } from './auth.actions';
import { User } from 'app/core/model/user.model';

export class AuthStateModel {
  token?: any;
  user?: User;
}

@State<AuthStateModel>({
  name: 'auth'
})
export class AuthState {
  constructor(private authService: AuthService) {}

  @Selector()
  static getToken(state: AuthStateModel) {
    return state.token;
  }

  @Selector()
  static getProfile(state: AuthStateModel) {
    return state.user;
  }

  @Action(Login)
  login({ patchState, dispatch }: StateContext<AuthStateModel>, { payload }: Login) {
    return this.authService.login(payload).pipe(
      tap((result: IAuth) => {
        patchState({
          token: result.token,
          user: result.user
        });
        dispatch(new Auth(result));
      })
    );
  }

  @Action(Auth)
  auth({ getState, patchState, dispatch }: StateContext<AuthStateModel>, { payload }: Auth) {
    dispatch(new Navigate(['/dashboard']));
  }

  @Action(Logout)
  logout({ setState, dispatch }: StateContext<AuthStateModel>) {
    setState({});
    dispatch(new LoginRedirect());
  }

  @Action(LoginRedirect)
  onLoginRedirect({ setState, dispatch }: StateContext<AuthStateModel>) {
    console.log('onLoginRedirect, navigating to /login');
    setState({});
    dispatch(new Navigate(['/']));
  }
}
