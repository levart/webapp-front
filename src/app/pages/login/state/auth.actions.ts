import { ILogin, IAuth } from 'app/core/model/auth';
import { User } from 'app/core/model/user.model';

export class Login {
  static readonly type = '[AUTH] Login user';
  constructor(public payload: ILogin) {}
}

export class Auth {
  static readonly type = '[AUTH] Auth user';
  constructor(public payload: IAuth) {}
}

export class Logout {
  static type = '[Auth] Logout';
}
export class LogoutSuccess {
  static type = '[Auth] LogoutSuccess';
}

// Events
export class LoginRedirect {
  static type = '[Auth] LoginRedirect';
}

export class LoginSuccess {
  static type = '[Auth] LoginSuccess';
  constructor(public user: User) {}
}
export class LoginFailed {
  static type = '[Auth] LoginFailed';
  constructor(public error: any) {}
}
