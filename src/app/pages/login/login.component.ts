import {Component, OnInit, ViewEncapsulation, OnDestroy} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

import {FuseConfigService} from '@fuse/services/config.service';
import {fuseAnimations} from '@fuse/animations';

import {UserAuth} from 'app/core/model/user-auth.model';
import {AuthService} from 'app/core/service/auth/auth.service';
import {Store, Select} from '@ngxs/store';
import {AuthState} from './state/auth.state';
import {Observable, Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {Login} from './state/auth.actions';
import {Router, ActivatedRoute} from '@angular/router';
import {Navigate} from '@ngxs/router-plugin';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations
})
export class LoginComponent implements OnInit, OnDestroy {

  destroy$: Subject<boolean> = new Subject<boolean>();

  loginForm: FormGroup;
  userAuth: UserAuth;

  constructor(
    private _fuseConfigService: FuseConfigService,
    private _formBuilder: FormBuilder,
    private store: Store,
    private router: Router
  ) {
    // Configure the layout
    this._fuseConfigService.config = {
      layout: {
        navbar: {
          hidden: true
        },
        toolbar: {
          hidden: true
        },
        footer: {
          hidden: true
        },
        sidepanel: {
          hidden: true
        }
      }
    };
  }

  ngOnInit(): void {
    this.loginForm = this._formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  authenticate(): void {
    if (this.loginForm.valid) {
      const email: string = this.loginForm.value.email;
      const password: string = this.loginForm.value.password;

      email.trim();
      password.trim();

      const payload = {
        email,
        password
      };
      this.store.dispatch(new Login(payload));
    }
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }
}
