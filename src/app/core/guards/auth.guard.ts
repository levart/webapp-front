import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Navigate } from '@ngxs/router-plugin';
import { Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { AuthState } from 'app/pages/login/state/auth.state';
import { map } from 'rxjs/operators';
import { LoginRedirect } from 'app/pages/login/state/auth.actions';
import { isNullOrUndefined } from 'util';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private store: Store) {}

  canActivate(): boolean {
    const isAuthenticated = this.store.selectSnapshot(AuthState.getToken);
    console.log(isAuthenticated);
    if (!isAuthenticated) {
      this.store.dispatch(new Navigate(['/dashboard']));
      return false;
    }
    return true;
  }
}
