import {
  HttpBackend,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { first, flatMap } from 'rxjs/operators';
import { AuthState } from 'app/pages/login/state/auth.state';


@Injectable()
export class TokenInterceptorService implements HttpInterceptor {
  constructor(private http: HttpBackend, private store: Store) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return this.store.select(AuthState.getToken).pipe(
      first(),
      flatMap(token => {
        const authReq = !!token
          ? req.clone({
              setHeaders: { Authorization: 'Bearer ' + token.accessToken }
            })
          : req;
        return next.handle(authReq);
      })
    );
  }
}
