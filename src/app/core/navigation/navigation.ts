import {FuseNavigation} from '@fuse/types';

export const navigation: FuseNavigation[] = [
  {
    id: 'applications',
    title: 'მენიუ',
    translate: 'NAV.APPLICATIONS',
    type: 'group',
    children: [
      {
        id: 'dashboards',
        title: 'ადმინისტრაცია',
        translate: 'NAV.DASHBOARDS',
        type: 'item',
        icon: 'people',
        url: '/users'
      },
      {
        id: 'dashboards',
        title: 'სალარო',
        translate: 'NAV.CASHIER',
        type: 'item',
        icon: 'money',
        url: '/cashier'
      },
      {
        id: 'dashboards',
        title: 'HR',
        translate: 'NAV.DASHBOARDS',
        type: 'collapsable',
        icon: 'monetization_on',
        children: [
          {
            id: 'users',
            title: 'თანამშრომლები',
            type: 'item',
            url: '/employees'
          },
          {
            id: 'salary',
            title: 'ხელფასები',
            type: 'item',
            url: '/salary'
          }
        ]
      },
      {
        id: 'services',
        title: 'პროცედურები',
        translate: 'NAV.DASHBOARDS',
        type: 'item',
        icon: 'apps',
        url: '/services'
      },

      {
        id: 'dashboards',
        title: 'პროდუქცია ',
        translate: 'NAV.DASHBOARDS',
        type: 'collapsable',
        icon: 'shop_two',
        children: [
          {
            id: 'products',
            title: 'სრული სია',
            type: 'item',
            url: '/products'
          },
          {
            id: 'realizations',
            title: 'რეალიზაცია',
            type: 'item',
            url: '/realizations'
          }
        ]
      },
      {
        id: 'dashboards',
        title: 'კლიენტები',
        translate: 'NAV.DASHBOARDS',
        type: 'item',
        icon: 'business',
        url: '/customers'
      },
      {
        id: 'calendars',
        title: 'კალენდარი',
        translate: 'NAV.DASHBOARDS',
        type: 'collapsable',
        icon: 'perm_contact_calendar',
        children: [
          {
            id: 'daily',
            title: 'დღის ფორმა',
            type: 'item',
            url: '/calendars/daily'
          },
          {
            id: 'month',
            title: 'თვის ფორმა',
            type: 'item',
            url: '/calendars/month'
          }
        ]
      },
      {
        id: 'dashboards',
        title: 'სტატისტიკა',
        translate: 'NAV.DASHBOARDS',
        type: 'collapsable',
        icon: 'insert_chart',
        children: [
          {
            id: 'users',
            title: 'დეტალური ძებნა',
            type: 'item',
            url: '/users'
          },
          {
            id: 'salary',
            title: 'გრაფიკული დიაგრამები',
            type: 'item',
            url: '/aa'
          }
        ]
      },
      {
        id: 'dashboards',
        title: 'ტრანზაქციები',
        translate: 'NAV.DASHBOARDS',
        type: 'collapsable',
        icon: 'euro_symbol',
        children: [
          {
            id: 'transactions',
            title: 'ისტორია',
            type: 'item',
            url: '/transactions/all',
            exactMatch: true
          },
          {
            id: 'deletedtransactions',
            title: 'წაშლილები',
            type: 'item',
            url: '/transactions/deleted',
            exactMatch: true
          }
        ]
      },
      {
        id: 'dashboards',
        title: 'კომუნიკაცია',
        translate: 'NAV.DASHBOARDS',
        type: 'collapsable',
        icon: 'alternate_email',
        children: [
          {
            id: 'users',
            title: 'Sms-ის გაგზავნა',
            type: 'item',
            url: '/users'
          },
          {
            id: 'salary',
            title: 'გზავნილების ისტორია',
            type: 'item',
            url: '/aa'
          }
        ]
      }
    ]
  }
];
