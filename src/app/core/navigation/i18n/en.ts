export const locale = {
  lang: 'en',
  data: {
    NAV: {
      APPLICATIONS: 'ნავიგაცია',
      DASHBOARDS: 'მთავარი'
    }
  },
  COLUMNS: {
    name: 'დასახელება',
    legalForm: 'სამართლებრივი ფორმა',
    identityNumber: 'საიდენტიფიკაცი #',
    phone: 'ტელეფონი',
    color: 'ფერი',
    information: 'ინფორმაცია',
    updated_at: 'განახლებული'
  }
};
