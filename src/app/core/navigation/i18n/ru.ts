export const locale = {
  lang: 'ru',
  data: {
    NAV: {
      APPLICATIONS: 'Programlar',
      DASHBOARDS: 'მთავარი'
    },
    COLUMNS: {
      name: 'დასახელება',
      legalForm: 'სამართლებრივი ფორმა',
      identityNumber: 'საიდენტიფიკაცი #',
      phone: 'ტელეფონი',
      information: 'ინფორმაცია',
      updated_at: 'განახლებული'
    }
  }
};
