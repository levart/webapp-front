export const locale = {
  lang: 'ka',
  data: {
    NAV: {
      APPLICATIONS: 'მენიუ',
      DASHBOARDS: 'მთავარი',
      CASHIER: 'სალარო'
    },
    COLUMNS: {
      name: 'დასახელება',
      legalForm: 'სამართლებრივი ფორმა',
      identityNumber: 'საიდენტიფიკაცი #',
      mobilePhone: 'მობილურის ნომერი',
      phone: 'ტელეფონი',
      information: 'ინფორმაცია',
      createdAt: 'შექმნილი',
      firstname: 'სახელი',
      lastname: 'გვარი',
      gender: 'სქესი',
      birthDate: 'დაბადების თარიღი',
      position: 'პოზიცია',
      productCategory: 'კატეგორია',
      distributor: 'მომწოდებელი',
      quantity: 'რაოდენობა',
      sum: 'ჯამი',
      grSum: 'ჯამი(გრამი)',
      grPerItem: 'გრამი ცალში',
      pricePerItem: 'მისაღები ფასი',
      salePricePerItem: 'გასაყიდი ფასი',
      buyDate: 'შექმნის თარიღი',
      transactionType: 'ტრანზაქციის ტიპი',
      employee: 'თანამშრომელი',
      service: 'სერვისი',
      price: 'ღირებულება',
      salePercent: 'ფასდაკლების %',
      fixedPrice: 'ფიქსირებული %/₾',
      fixedSpend: 'ფიქსირებული დანახარჯი',
      barcode: 'ბარკოდი',
      productName: 'პროდუქტის დასახელება',
      productVolume: 'პროდუქტის მოცულობა',
      peymentType: 'გადახდის ტიპი',
      createdBy: 'ვინ შექმნა',
      status: 'სტატუსი',
      unitGram: 'მოცულობა',
      unitQuantity: 'რაოდენობა',
      employeePercent: 'თანამშრომლის %',
      employeFixedPrice: 'ფიქსირებული თანხა',
      time: 'დრო',
      isSpecial: 'სპეციალური',
      autoPriceView: 'ფასი ავტომატურად',
      autoTimeView: 'დრო ავტომატურად',
      color: 'ფერი',
      category: 'კატეგორია',
    },
    PAYMENTTYPE: {
      SELL: 'გაყიდვა',
      SPEND: 'გახარჯვა'
    },
    PAGINATION: {
      page: 'გვერდი',
      pageCount: 'სულ'
    }
  }
};
