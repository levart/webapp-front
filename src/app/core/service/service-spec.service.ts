import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {ApiService} from './api/api.service';

@Injectable({
  providedIn: 'root'
})
export class ServiceSpecService {

  constructor(private apiservice: ApiService) {
  }


  get(params): Observable<any> {
    return this.apiservice.get<any>(`service-specs`, params);
  }

  getById(serviceId): Observable<any> {
    return this.apiservice.post<any>(`service-specs`, {serviceId});
  }

  getByService(serviceId): Observable<any> {
    return this.apiservice.post<any>(`service-specs/getByService`, {serviceId});
  }

  update(model: any): Observable<any> {
    return this.apiservice.put(`service-specs/${model.id}`, model);
  }

  create(model: any): Observable<any> {
    return this.apiservice.post(`service-specs`, model);
  }

  delete(employeeId: any): Observable<any> {
    console.log(employeeId);
    return this.apiservice.delete(`service-specs/${employeeId}`);
  }
}
