import {Injectable} from '@angular/core';
import {ApiService} from '../api/api.service';
import {IAuth, ILogin} from '../../model/auth';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  constructor(private apiservice: ApiService) {
  }

  getById(employeeId): Observable<any> {
    return this.apiservice.get<any>(`employee/${employeeId}`);
  }

  getEmployees(params: any = {}): Observable<any> {
    return this.apiservice.get<any>(`employee`, params);
  }

  deleteEmployee(employeeId): Observable<any> {
    return this.apiservice.delete<any>(`employee/${employeeId}`);
  }

  update(model: any): Observable<any> {
    return this.apiservice.put(`employee/${model.id}`, model);
  }

  create(model: any): Observable<any> {
    return this.apiservice.post(`employee`, model);
  }

  delete(employeeId: any): Observable<any> {
    console.log(employeeId);
    return this.apiservice.delete(`employee/${employeeId}`);
  }

  getDropdown(): Observable<any> {
    return this.apiservice.post<any>(`employee/dropdown`);
  }
}
