import {Injectable} from '@angular/core';
import {ApiService} from '../api/api.service';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ServiceService {

  constructor(private apiservice: ApiService) {
  }

  getById(employeeId): Observable<any> {
    return this.apiservice.get<any>(`service/${employeeId}`);
  }

  getEmployees(params: any = {}): Observable<any> {
    return this.apiservice.get<any>(`service`, params);
  }

  deleteEmployee(employeeId): Observable<any> {
    return this.apiservice.delete<any>(`service/${employeeId}`);
  }

  update(model: any): Observable<any> {
    return this.apiservice.put(`service/${model.id}`, model);
  }

  create(model: any): Observable<any> {
    return this.apiservice.post(`service`, model);
  }

  delete(employeeId: any): Observable<any> {
    console.log(employeeId);
    return this.apiservice.delete(`service/${employeeId}`);
  }

  dropdown(params = {}): Observable<any> {
    return this.apiservice.post<any>(`service/dropdown`, params);
  }

  userServices(params = {}): Observable<any> {
    return this.apiservice.post<any>(`service/userServices`, params);
  }

  special(params = {}): Observable<any> {
    return this.apiservice.post<any>(`service/special`, params);
  }
}
