import {Injectable} from '@angular/core';
import {ApiService} from '../api/api.service';
import {Observable} from 'rxjs';
import {IAuth} from 'app/core/model/auth';

@Injectable({
  providedIn: 'root'
})
export class PositionService {
  constructor(private apiservice: ApiService) {
  }

  getDropdown(): Observable<any> {
    return this.apiservice.get<any>(`positions/dropdown`);
  }

  getPositions(params: any): Observable<any> {
    return this.apiservice.get<any>(`positions`, params);
  }

  addPosition(name): Observable<any> {
    return this.apiservice.post<any>(`positions`, {name});
  }

  deletePosition(id) {
    return this.apiservice.delete<any>(`positions/${id}`);
  }

  updatePosition(id, data) {
    return this.apiservice.put<any>(`positions/${id}`, data);
  }
}
