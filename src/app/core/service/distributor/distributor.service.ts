import {Injectable} from '@angular/core';
import {ApiService} from '../api/api.service';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DistributorService {
  private endpoint = 'distributor';

  constructor(private apiService: ApiService) {
  }

  get(params): Observable<any> {
    return this.apiService.get<any>(`${this.endpoint}`, params);
  }

  dropdown(params): Observable<any> {
    return this.apiService.post<any>(`${this.endpoint}/dropdown`, params);
  }

  getById(id): Observable<any> {
    return this.apiService.get<any>(`${this.endpoint}/${id}`);
  }

  create(params): Observable<any> {
    return this.apiService.post<any>(`${this.endpoint}`, params);
  }

  update(params): Observable<any> {
    return this.apiService.put<any>(`${this.endpoint}`, params);
  }

  delete(id): Observable<any> {
    return this.apiService.delete<any>(`${this.endpoint}/${id}`);
  }
}
