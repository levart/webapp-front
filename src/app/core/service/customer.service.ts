import { Injectable } from '@angular/core';
import {ApiService} from './api/api.service';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  constructor(private apiservice: ApiService) {
  }

  getById(employeeId): Observable<any> {
    return this.apiservice.get<any>(`users/${employeeId}`);
  }

  getAll(params: any = {}): Observable<any> {
    return this.apiservice.get<any>(`users`, params);
  }

  deleteEmployee(employeeId): Observable<any> {
    return this.apiservice.delete<any>(`users/${employeeId}`);
  }

  update(model: any): Observable<any> {
    return this.apiservice.put(`users/${model.id}`, model);
  }

  create(model: any): Observable<any> {
    return this.apiservice.post(`users`, model);
  }

  delete(employeeId: any): Observable<any> {
    console.log(employeeId);
    return this.apiservice.delete(`users/${employeeId}`);
  }

  search(params): Observable<any> {
    return this.apiservice.post<any>(`users/search`, {search: params});
  }
}
