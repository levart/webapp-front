import {Injectable} from '@angular/core';
import {ApiService} from './api/api.service';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ServiceProductsService {

  private endpoint = 'service-products';

  constructor(private apiService: ApiService) {
  }

  get(params): Observable<any> {
    return this.apiService.get<any>(`${this.endpoint}`, params);
  }

  getById(id): Observable<any> {
    return this.apiService.get<any>(`${this.endpoint}/${id}`);
  }

  getByService(serviceId): Observable<any> {
    return this.apiService.post<any>(`${this.endpoint}/getByService`, {serviceId});
  }

  create(params): Observable<any> {
    return this.apiService.post<any>(`${this.endpoint}`, params);
  }

  update(params): Observable<any> {
    return this.apiService.put<any>(`${this.endpoint}`, params);
  }

  delete(id): Observable<any> {
    return this.apiService.delete<any>(`${this.endpoint}/${id}`);
  }

  search(params): Observable<any> {
    return this.apiService.post<any>(`${this.endpoint}/search`, {search: params});
  }

  dropdown(params = {}): Observable<any> {
    return this.apiService.post<any>(`${this.endpoint}/dropdown`, params);
  }
}
