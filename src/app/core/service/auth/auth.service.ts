import { Injectable } from "@angular/core";
import { ApiService } from "../api/api.service";
import { Observable } from "rxjs";
import { ILogin, IAuth } from "app/core/model/auth";

@Injectable({
  providedIn: "root"
})
export class AuthService {
  constructor(private apiservice: ApiService) {}

  login(params: ILogin): Observable<IAuth> {
    return this.apiservice.post<IAuth>(`auth/login`, params);
  }
}
