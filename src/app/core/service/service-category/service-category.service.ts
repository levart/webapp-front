import { Injectable } from '@angular/core';
import {ApiService} from '../api/api.service';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ServiceCategoryService {

  private endpoint = 'service-category';

  constructor(private apiservice: ApiService) {
  }

  getDropdown(): Observable<any> {
    return this.apiservice.get<any>(`${this.endpoint}/dropdown`);
  }

  getPagination(params: any): Observable<any> {
    return this.apiservice.get<any>(`${this.endpoint}`, params);
  }

  create(params): Observable<any> {
    return this.apiservice.post<any>(`${this.endpoint}`, params);
  }

  delete(id) {
    return this.apiservice.delete<any>(`${this.endpoint}/${id}`);
  }

  update(id, data) {
    return this.apiservice.put<any>(`${this.endpoint}/${id}`, data);
  }

  dropdown(params = {}): Observable<any> {
    return this.apiservice.post<any>(`${this.endpoint}/dropdown`, params);
  }



}
