import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {ApiService} from './api/api.service';

@Injectable({providedIn: 'root'})
export class UserService {
  constructor(private apiservice: ApiService) {
  }

  getById(employeeId): Observable<any> {
    return this.apiservice.get<any>(`users/${employeeId}`);
  }

  getAll(params: any = {}): Observable<any> {
    return this.apiservice.get<any>(`users`, params);
  }

  deleteEmployee(employeeId): Observable<any> {
    return this.apiservice.delete<any>(`users/${employeeId}`);
  }

  update(model: any): Observable<any> {
    return this.apiservice.put(`users/${model.id}`, model);
  }

  create(model: any): Observable<any> {
    return this.apiservice.post(`users`, model);
  }

  delete(employeeId: any): Observable<any> {
    console.log(employeeId);
    return this.apiservice.delete(`users/${employeeId}`);
  }
}
