export interface Timestamp {
  created: Date;
  updated: Date;
}

export interface Organisation {
  id: number;
  timestamp: Timestamp;
  commersantPID?: any;
  parent?: any;
  name: string;
  idcode: string;
  jAddress: string;
  address: string;
  phone1: string;
  phone2: string;
  ltd: string;
  director: string;
  logo?: any;
  images?: any;
  pricePerDay: number;
  facebook?: any;
  twitter?: any;
  google?: any;
  smsCount: number;
  note?: any;
  whStart: string;
  whEnd: string;
  location: string;
  smsUser: string;
  smsPass: string;
  smsClientId: string;
  smsServiceId: string;
  languagePack: string;
  trial: boolean;
  active: boolean;
  disableReason?: any;
  trialLastDate: string;
  superAdmin: number;
  testAccount: number;
  organisationPayment?: any;
}

export interface Timestamp2 {
  created: Date;
  updated?: any;
}

export interface Timestamp3 {
  created: Date;
  updated: Date;
}

export interface Organisation2 {
  id: number;
  timestamp: Timestamp3;
  commersantPID?: any;
  parent?: any;
  name: string;
  idcode: string;
  jAddress: string;
  address: string;
  phone1: string;
  phone2: string;
  ltd: string;
  director: string;
  logo?: any;
  images?: any;
  pricePerDay: number;
  facebook?: any;
  twitter?: any;
  google?: any;
  smsCount: number;
  note?: any;
  whStart: string;
  whEnd: string;
  location: string;
  smsUser: string;
  smsPass: string;
  smsClientId: string;
  smsServiceId: string;
  languagePack: string;
  trial: boolean;
  active: boolean;
  disableReason?: any;
  trialLastDate: string;
  superAdmin: number;
  testAccount: number;
  organisationPayment?: any;
}

export interface Timestamp4 {
  created: Date;
  updated?: any;
}

export interface Permission {
  id: number;
  timestamp: Timestamp4;
  name: string;
  description: string;
  isSuperAdmin: number;
}

export interface Role {
  id: number;
  timestamp: Timestamp2;
  organisation: Organisation2;
  role: string;
  permissions: Permission[];
}

export interface Authority {
  authority: string;
}

export interface User {
  id: number;
  timestamp?: any;
  organisation: Organisation;
  role: Role;
  userName: string;
  password: string;
  firstname: string;
  lastname: string;
  email: string;
  dob: Date;
  pid: string;
  phone: string;
  active: boolean;
  salaryCreated: boolean;
  authorities: Authority[];
  enabled: boolean;
  username: string;
  accountNonLocked: boolean;
  accountNonExpired: boolean;
  credentialsNonExpired: boolean;
}
