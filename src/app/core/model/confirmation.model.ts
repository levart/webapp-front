export interface ConfirmationDialog {
  message: string;
  title: string;
}
