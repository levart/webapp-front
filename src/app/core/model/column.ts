export interface Column {
  columnName: string;
  columnTitle: string;
  columnType: string;
  objectName?: any;
  width?: string;
}
