export class PageParams {
  constructor(
    public page: number,
    public pageSize: number,
    public itemCount?: number,
    public pageCount?: number,
    public isDeleted?: boolean
  ) { }
}
