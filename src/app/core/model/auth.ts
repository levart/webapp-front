import { User } from './user.model';

export interface ILogin {
  email: string;
  password: string;
}

export interface IAuth {
  token: string;
  user: User;
}
