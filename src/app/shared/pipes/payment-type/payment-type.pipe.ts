import {Pipe, PipeTransform} from '@angular/core';
import memo from 'memo-decorator';
@Pipe({
  name: 'paymentType',
  pure: true
})
export class PaymentTypePipe implements PipeTransform {

  @memo()
  transform(value: any, ...args: any[]): any {
    console.log(value);
    if (value === 1) {
      return 'PAYMENTTYPE.SELL';
    }
    if (value === 2) {
      return 'PAYMENTTYPE.SPEND';
    }
    return value;
  }

}
