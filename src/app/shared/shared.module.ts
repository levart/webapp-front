import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ConfirmationDialogComponent} from './components/confirmation-dialog/confirmation-dialog.component';
import {TranslateModule} from '@ngx-translate/core';
import {DynamicMatTableComponent} from './components/dynamic-mat-table/dynamic-mat-table.component';
import {PaginatorComponent} from './components/paginator/paginator.component';
import {MaterialModule} from './modules/material.module';
import {FormlyModule} from '@ngx-formly/core';
import {FormlyMaterialModule} from '@ngx-formly/material';
import {ColorPickerModule} from 'ngx-color-picker';
import {NgxMaterialTimepickerModule} from 'ngx-material-timepicker';
import {WaInputComponent} from './components/wa-input/wa-input.component';
import {WaDatepickerComponent} from './components/wa-datepicker/wa-datepicker.component';
import {BsDatepickerModule} from '@bit/valor-software.ngx-bootstrap.datepicker';
import {WaDatepickerRangeComponent} from './components/wa-datepicker-range/wa-datepicker-range.component';
import { WaTextareaComponent } from './components/wa-textarea/wa-textarea.component';
import { WaButtonComponent } from './components/wa-button/wa-button.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { WaEmployeeSelectComponent } from './components/wa-employee-select/wa-employee-select.component';
import { WaBookingStatusSelectComponent } from './components/wa-booking-status-select/wa-booking-status-select.component';
import { WaPaymentTypeComponent } from './components/wa-payment-type/wa-payment-type.component';
import { WaServiceSelectComponent } from './components/wa-service-select/wa-service-select.component';
import {ProductSearchComponent} from './components/product-search/product-search.component';
import { PaymentTypePipe } from './pipes/payment-type/payment-type.pipe';
import { PaginationComponent } from './components/pagination/pagination.component';
import {FlexModule} from '@angular/flex-layout';


@NgModule({
  declarations: [
    ConfirmationDialogComponent,
    DynamicMatTableComponent,
    PaginatorComponent,
    WaInputComponent,
    WaDatepickerComponent,
    WaDatepickerRangeComponent,
    WaTextareaComponent,
    WaButtonComponent,
    WaEmployeeSelectComponent,
    WaBookingStatusSelectComponent,
    WaPaymentTypeComponent,
    WaServiceSelectComponent,
    ProductSearchComponent,
    PaymentTypePipe,
    PaginationComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    TranslateModule,
    MaterialModule,
    ColorPickerModule,
    NgxMaterialTimepickerModule,
    BsDatepickerModule,
    ReactiveFormsModule,
    FlexModule
  ],
  exports: [
    FormsModule,
    ReactiveFormsModule,
    TranslateModule,
    MaterialModule,
    ColorPickerModule,
    BsDatepickerModule,

    ConfirmationDialogComponent,
    DynamicMatTableComponent,
    PaginatorComponent,
    FormlyModule,
    FormlyMaterialModule,
    NgxMaterialTimepickerModule,
    WaInputComponent,
    WaDatepickerComponent,
    WaDatepickerRangeComponent,
    WaTextareaComponent,
    WaButtonComponent,
    WaEmployeeSelectComponent,
    WaBookingStatusSelectComponent,
    WaPaymentTypeComponent,
    WaServiceSelectComponent,
    ProductSearchComponent,
    PaymentTypePipe,
    PaginationComponent
  ],
  entryComponents: [ConfirmationDialogComponent]
})
export class SharedModule {
}
