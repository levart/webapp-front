import {
  ChangeDetectionStrategy,
  Component, ContentChild,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output, SimpleChange,
  SimpleChanges, ViewChild
} from '@angular/core';

import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';

import map from 'lodash/map';
import {TranslateService} from '@ngx-translate/core';
import {ActivatedRoute, Router} from '@angular/router';
import {PageParams} from '../../../core/model/page-param';
import {Column} from '../../../core/model/column';
import {Paginator} from '../../../core/model/paginator';


@Component({
  selector: 'app-dynamic-mat-table',
  templateUrl: './dynamic-mat-table.component.html',
  styleUrls: ['./dynamic-mat-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DynamicMatTableComponent implements OnInit, OnChanges {
  @Input() actions: any;
  @Input() data: any;
  @Input() isLoading: boolean;
  @Input() pageParams: PageParams;
  @Input() columns: Column[];

  @Output() pageParamsChange: EventEmitter<PageParams> = new EventEmitter<PageParams>();
  @Output() show: EventEmitter<any> = new EventEmitter<any>();
  @Output() edit: EventEmitter<any> = new EventEmitter<any>();
  @Output() delete: EventEmitter<any> = new EventEmitter<any>();
  @Output() accept: EventEmitter<any> = new EventEmitter<any>();


  dataSource = new MatTableDataSource([]);
  displayedColumns: string[] = [];
  paginatorLength = 0;
  paginatorPageSize = 10;
  paginatorPageIndex = 1;
  @ContentChild(MatPaginator, {static: false}) paginator: MatPaginator;

  constructor(
    public translate: TranslateService,
    private router: Router,
    private route: ActivatedRoute
  ) {
  }

  ngOnInit() {
    const {page, pageSize} = this.pageParams;
    this.paginatorPageIndex = page;
    this.paginatorPageSize = pageSize;
  }

  ngOnChanges(changes: SimpleChanges): void {
    const currentItem: SimpleChange = changes.data;

    if (currentItem && currentItem.currentValue) {
      this.dataSource = new MatTableDataSource(currentItem.currentValue.data);
      this.paginatorLength = currentItem.currentValue.meta.itemCount;
      this.dataSource.paginator = this.paginator;
      this.getTransformedColumns(this.columns);

    }
  }

  getTransformedColumns(columns) {
    const transformColumns = map(columns, 'columnName');

    this.displayedColumns = [
      ...transformColumns,
      'actions'
    ];
  }

  paginatorChange(pageEvent: Paginator) {
    this.pageParamsChange.emit({
      page: pageEvent.pageIndex,
      pageSize: pageEvent.pageSize
    });
    // this.router.navigate(['.'], {queryParams: {page: pageEvent.pageIndex, pagesize: pageEvent.pageSize}, relativeTo: this.route});
  }

  showRow(event) {
    this.show.emit({event});
  }

  editRow(event) {
    this.edit.emit({event});
  }

  deleteRow(event) {
    this.delete.emit({event});
  }

  acceptRow(event) {
    this.accept.emit({event});
  }


  booleanDot(status) {
    if (status) {
      return 'booleanTrue';
    }
    return 'booleanFalse';
  }
}


