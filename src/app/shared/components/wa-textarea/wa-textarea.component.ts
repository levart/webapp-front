import {Component, Input, OnInit} from '@angular/core';
import {FormControlName, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-wa-textarea',
  templateUrl: './wa-textarea.component.html',
  styleUrls: ['./wa-textarea.component.scss']
})
export class WaTextareaComponent implements OnInit {
  @Input() group: FormGroup;
  @Input() controlName: FormControlName;
  @Input() label;
  @Input() placeholder;
  @Input() cols;
  @Input() rows;
  constructor() { }

  ngOnInit() {
  }

}
