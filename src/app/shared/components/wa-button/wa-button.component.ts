import {Component, Input, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';

@Component({
  selector: 'app-wa-button',
  templateUrl: './wa-button.component.html',
  styleUrls: ['./wa-button.component.scss']
})
export class WaButtonComponent implements OnInit {

  @Input() label;
  @Input() name;
  @Input() iconName;
  @Input() color: ButtonColor = ButtonColor.blue;
  constructor() { }

  ngOnInit() {
  }

  setColor(color: any) {
  switch (color) {
    case 'blue':
      return 'blue-but';
    case 'orange':
      return 'orange-but'
  }
  }
}

export enum ButtonColor {
  blue,
  orange
}
