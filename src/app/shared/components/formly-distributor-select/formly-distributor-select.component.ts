import { Component, OnInit } from '@angular/core';
import {FieldType} from '@ngx-formly/core';
import {Observable} from 'rxjs';
import {PositionService} from '../../../core/service/position/position.service';
import {DistributorService} from '../../../core/service/distributor/distributor.service';

@Component({
  selector: 'app-formly-distributor-select',
  templateUrl: './formly-distributor-select.component.html',
  styleUrls: ['./formly-distributor-select.component.scss']
})
export class FormlyDistributorSelectComponent  extends FieldType implements OnInit {
  distributors$: Observable<any>;

  constructor(private service: DistributorService) {
    super();
  }

  ngOnInit() {
    this.getPositions();
  }

  getPositions() {
    this.distributors$ = this.service.dropdown({});
  }

}

