import { Component, OnInit } from '@angular/core';
import {FieldType} from '@ngx-formly/core';

@Component({
  selector: 'app-formly-timepiker',
  templateUrl: './formly-timepiker.component.html',
  styleUrls: ['./formly-timepiker.component.scss']
})
export class FormlyTimepikerComponent  extends FieldType implements OnInit {

  constructor() {
    super();
  }

  ngOnInit() {
  }

}
