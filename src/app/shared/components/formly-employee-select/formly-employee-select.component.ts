import {Component, OnInit} from '@angular/core';
import {FieldType} from '@ngx-formly/core';
import {Observable} from 'rxjs';
import {EmployeeService} from '../../../core/service/employee/employee.service';

@Component({
  selector: 'app-formly-employee-select',
  templateUrl: './formly-employee-select.component.html',
  styleUrls: ['./formly-employee-select.component.scss']
})
export class FormlyEmployeeSelectComponent extends FieldType implements OnInit {
  employees$: Observable<any>;

  constructor(private service: EmployeeService) {
    super();
  }

  ngOnInit() {
    this.getEmployees();
  }

  getEmployees() {
    this.employees$ = this.service.getDropdown();
  }

}
