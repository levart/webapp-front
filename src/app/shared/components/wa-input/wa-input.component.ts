import {Component, Input, OnInit} from '@angular/core';
import {FormControlName, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-wa-input',
  templateUrl: './wa-input.component.html',
  styleUrls: ['./wa-input.component.scss']
})
export class WaInputComponent implements OnInit {
  @Input() group: FormGroup;
  @Input() controlName: FormControlName;
  @Input() label  = null;
  @Input() placeholder = '';
  @Input() type = 'text';

  constructor() { }

  ngOnInit() {
  }

}
