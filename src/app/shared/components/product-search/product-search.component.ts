import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, FormGroupName} from '@angular/forms';
import {Observable, of} from 'rxjs';
import {CustomerService} from '../../../core/service/customer.service';
import {ProductService} from '../../../core/service/product/product.service';

@Component({
  selector: 'app-product-search',
  templateUrl: './product-search.component.html',
  styleUrls: ['./product-search.component.scss']
})
export class ProductSearchComponent implements OnInit {
  @Input() group: FormGroup;
  @Output() selectChanged: EventEmitter<any> = new EventEmitter<any>();
  @Output() addUserEvent: EventEmitter<boolean> = new EventEmitter<boolean>();
  searchForm: FormGroup;
  search;
  selected;

  customers = [
    {
      firstname: 'Levan',
      lastname: 'Jmukhadze',
      phone: '555661277'
    },
    {
      firstname: 'Levan',
      lastname: 'Meskhishvili',
      phone: '555003232'
    }
  ];
  searchResult$: Observable<any>;

  constructor(
    private fb: FormBuilder,
    private productService: ProductService
  ) {}

  ngOnInit() {}

  searchEvent(event: string) {
    if (event && event.length > 1) {
      this.searchResult$ = this.productService.search(event);
    } else {
      this.searchResult$ = of(null);
    }
  }

  selectItem(item: any) {
    this.selected = item;
    this.searchResult$ = of(null);
    this.selectChanged.emit(this.selected);
    this.search = null;
  }

  clearCustomer() {
    this.selected = null;
    this.addUserEvent.emit(false);
    this.search = null;
  }

  addUser() {
    this.addUserEvent.emit(true);
    this.search = null;
  }

}
