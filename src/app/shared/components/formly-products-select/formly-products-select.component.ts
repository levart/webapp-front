import { Component, OnInit } from '@angular/core';
import {FieldType} from '@ngx-formly/core';
import {Observable} from 'rxjs';
import {ProductCategoryService} from '../../../core/service/product-category/product-category.service';
import {ProductService} from '../../../core/service/product/product.service';

@Component({
  selector: 'app-formly-products-select',
  templateUrl: './formly-products-select.component.html',
  styleUrls: ['./formly-products-select.component.scss']
})
export class FormlyProductsSelectComponent extends FieldType implements OnInit {
  distributors$: Observable<any>;

  constructor(private service: ProductService) {
    super();
  }

  ngOnInit() {
    this.getPositions();
  }

  getPositions() {
    this.distributors$ = this.service.dropdown({});
  }

}
