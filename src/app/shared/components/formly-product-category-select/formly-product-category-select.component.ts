import { Component, OnInit } from '@angular/core';
import {FieldType} from '@ngx-formly/core';
import {Observable} from 'rxjs';
import {DistributorService} from '../../../core/service/distributor/distributor.service';
import {ProductCategoryService} from '../../../core/service/product-category/product-category.service';

@Component({
  selector: 'app-formly-product-category-select',
  templateUrl: './formly-product-category-select.component.html',
  styleUrls: ['./formly-product-category-select.component.scss']
})
export class FormlyProductCategorySelectComponent  extends FieldType implements OnInit {
  distributors$: Observable<any>;

  constructor(private service: ProductCategoryService) {
    super();
  }

  ngOnInit() {
    this.getPositions();
  }

  getPositions() {
    this.distributors$ = this.service.dropdown({});
  }

}
