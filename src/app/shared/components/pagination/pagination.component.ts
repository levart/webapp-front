import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent implements OnInit {
  @Input() page: number;
  @Input() pageSize: number;
  @Input() pageCount: number;
  @Input() itemCount: number;
  @Output() paginatorChange: EventEmitter<any> = new EventEmitter<any>();


  constructor() {
  }

  ngOnInit() {
  }

  goPage(page: number) {
    this.page = page;
    this.changePagination();
  }

  prevPage(page: number) {
    if (page === 1) {
      this.changePagination();
      return;
    }

    this.page = --page;
    this.changePagination();
  }

  nextPage(page: number) {
    if (page === this.pageCount) {
      this.page = this.pageCount;
      this.changePagination();
      return;
    }
    this.page++;
    this.changePagination();
  }

  lastPage() {
    this.page = this.pageCount;
    this.changePagination();
  }

  changePagination() {
    console.log(this.pageSize);
    console.log(this.page);
    this.paginatorChange.emit({
      pageSize: this.pageSize,
      page: this.page
    });
  }

  pageSizeChange(event) {
    console.log(event);
    this.pageSize = event;
    this.changePagination();
  }

  startNumber() {
    return (this.page - 1) * this.pageSize + 1;
  }

  endNumber() {
    const lastSize = this.page * this.pageSize;
    if (this.itemCount < lastSize) {
      return this.itemCount;
    }
    return lastSize;
  }
}
