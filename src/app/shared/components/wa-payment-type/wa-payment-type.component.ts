import {Component, Input, OnInit} from '@angular/core';
import {FormControlName, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-wa-payment-type',
  templateUrl: './wa-payment-type.component.html',
  styleUrls: ['./wa-payment-type.component.scss']
})
export class WaPaymentTypeComponent implements OnInit {
  @Input() group: FormGroup;
  @Input() controlName: FormControlName;
  @Input() label;
  @Input() placeholder;
  @Input() type = 'text';
  constructor() { }

  ngOnInit() {
  }

}
