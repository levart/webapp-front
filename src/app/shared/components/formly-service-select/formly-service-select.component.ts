import {Component, OnInit} from '@angular/core';
import {FieldType} from '@ngx-formly/core';
import {Observable} from 'rxjs';
import {ServiceService} from '../../../core/service/service/service.service';

@Component({
  selector: 'app-formly-service-select',
  templateUrl: './formly-service-select.component.html',
  styleUrls: ['./formly-service-select.component.scss']
})
export class FormlyServiceSelectComponent  extends FieldType implements OnInit {
  distributors$: Observable<any>;

  constructor(private service: ServiceService) {
    super();
  }

  ngOnInit() {
    this.getPositions();
  }

  getPositions() {
    this.distributors$ = this.service.dropdown({});
  }

}
