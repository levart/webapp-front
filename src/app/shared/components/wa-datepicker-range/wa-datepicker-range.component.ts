import {Component, Input, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';

@Component({
  selector: 'app-wa-datepicker-range',
  templateUrl: './wa-datepicker-range.component.html',
  styleUrls: ['./wa-datepicker-range.component.scss']
})
export class WaDatepickerRangeComponent implements OnInit {
  @Input() group: FormGroup;
  @Input() label;
  @Input() placeholder;

  constructor() { }

  ngOnInit() {
  }

}
