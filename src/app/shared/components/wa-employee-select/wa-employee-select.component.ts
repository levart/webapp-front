import {Component, Input, OnInit} from '@angular/core';
import {FormControlName, FormGroup} from '@angular/forms';
import {Observable} from 'rxjs';
import {EmployeeService} from '../../../core/service/employee/employee.service';

@Component({
  selector: 'app-wa-employee-select',
  templateUrl: './wa-employee-select.component.html',
  styleUrls: ['./wa-employee-select.component.scss']
})
export class WaEmployeeSelectComponent implements OnInit {
  @Input() group: FormGroup;
  @Input() controlName: FormControlName;
  @Input() label;
  @Input() placeholder;
  @Input() type = 'text';
  employees$: Observable<any>;

  constructor(private service: EmployeeService) {
  }

  ngOnInit() {
    this.getEmployees();
  }

  getEmployees() {
    this.employees$ = this.service.getDropdown();
  }

}
