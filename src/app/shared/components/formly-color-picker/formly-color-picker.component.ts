import {Component, DoCheck, OnInit} from '@angular/core';
import {FieldType} from '@ngx-formly/core';

@Component({
  selector: 'app-formly-color-picker',
  templateUrl: './formly-color-picker.component.html',
  styleUrls: ['./formly-color-picker.component.scss']
})
export class FormlyColorPickerComponent extends FieldType implements OnInit, DoCheck {
  public color;
  constructor() {
    super();
  }

  ngOnInit() {
    this.color = this.field.defaultValue;
  }

  ngDoCheck() {
    this.model[this.field.key] = this.color;
  }
}
