import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Observable} from 'rxjs';
import {ServiceService} from '../../../core/service/service/service.service';
import {FormControlName, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-wa-service-select',
  templateUrl: './wa-service-select.component.html',
  styleUrls: ['./wa-service-select.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class WaServiceSelectComponent implements OnInit {
  @Input() group: FormGroup;
  @Input() controlName: FormControlName;
  @Input() label;
  @Input() placeholder;
  @Input() type = 'text';
  @Input() isSpecial = false;

  @Output() changed: EventEmitter<any> = new EventEmitter<any>();
  distributors$: Observable<any>;

  constructor(private service: ServiceService) {

  }

  ngOnInit() {
    this.getPositions();
  }

  getPositions() {
    this.distributors$ = this.service.dropdown({isSpecial: this.isSpecial });
  }


  change($event: Event) {
    console.log($event.currentTarget);
  }
}
