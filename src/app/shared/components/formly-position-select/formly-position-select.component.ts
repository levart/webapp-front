import { Component, OnInit } from '@angular/core';
import {FieldType} from '@ngx-formly/core';
import {Observable} from 'rxjs';
import {PositionService} from '../../../core/service/position/position.service';

@Component({
  selector: 'app-formly-position-select',
  templateUrl: './formly-position-select.component.html',
  styleUrls: ['./formly-position-select.component.scss']
})
export class FormlyPositionSelectComponent extends FieldType implements OnInit {
  positions$: Observable<any>;

  constructor(private service: PositionService) {
    super();
  }

  ngOnInit() {
    this.getPositions();
  }

  getPositions() {
    this.positions$ = this.service.getPositions({});
  }

}
