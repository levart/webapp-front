import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-paginator',
  templateUrl: './paginator.component.html',
  styleUrls: ['./paginator.component.scss']
})
export class PaginatorComponent implements OnInit {

  @Input() length = 0;

  @Input() pageSize: number;

  @Input() pageIndex = 1;

  @Output() page = new EventEmitter();

  pageSizeOptions: number[] = [1, 10, 25, 100, 250];

  constructor() {
  }

  ngOnInit() {
  }

  changed(event) {
    this.page.emit({
      pageSize: event.pageSize,
      pageIndex: event.pageIndex + 1
    });
  }

}
