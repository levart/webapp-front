import { Component, OnInit } from '@angular/core';
import {FieldType} from '@ngx-formly/core';
import {Observable, of} from 'rxjs';

@Component({
  selector: 'app-formly-sallary-type-select',
  templateUrl: './formly-sallary-type-select.component.html',
  styleUrls: ['./formly-sallary-type-select.component.scss']
})
export class FormlySallaryTypeSelectComponent extends FieldType implements OnInit {
  distributors$: Observable<any> = of([
    {
      value: 'FIXED',
      label: 'ფიქსირებული ხელფასი'
    },
    {
      value: 'OUTPUT',
      label: 'გამომუშავებაზე'
    },
    {
      value: 'ALL',
      label: 'ორივე'
    }
  ]);
  constructor() {
    super();
  }

  ngOnInit() {
  }

}
