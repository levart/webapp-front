import { Component, OnInit } from '@angular/core';
import {FieldType} from '@ngx-formly/core';
import {Observable} from 'rxjs';
import {ProductCategoryService} from '../../../core/service/product-category/product-category.service';
import {ServiceCategoryService} from '../../../core/service/service-category/service-category.service';

@Component({
  selector: 'app-formly-service-category-select',
  templateUrl: './formly-service-category-select.component.html',
  styleUrls: ['./formly-service-category-select.component.scss']
})
export class FormlyServiceCategorySelectComponent extends FieldType implements OnInit {
  distributors$: Observable<any>;

  constructor(private service: ServiceCategoryService) {
    super();
  }

  ngOnInit() {
    this.getPositions();
  }

  getPositions() {
    this.distributors$ = this.service.dropdown({});
  }

}
