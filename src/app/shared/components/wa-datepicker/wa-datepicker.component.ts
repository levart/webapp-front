import {Component, Input, OnInit} from '@angular/core';
import {FormControlName, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-wa-datepicker',
  templateUrl: './wa-datepicker.component.html',
  styleUrls: ['./wa-datepicker.component.scss']
})
export class WaDatepickerComponent implements OnInit {
  @Input() controlName: FormControlName;
  @Input() group: FormGroup;
  @Input() label;
  @Input() placeholder;

  constructor() { }

  ngOnInit() {
  }

}
