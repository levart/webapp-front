import {Component, Input, OnInit} from '@angular/core';
import {FormControlName, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-wa-booking-status-select',
  templateUrl: './wa-booking-status-select.component.html',
  styleUrls: ['./wa-booking-status-select.component.scss']
})
export class WaBookingStatusSelectComponent implements OnInit {

  @Input() group: FormGroup;
  @Input() controlName: FormControlName;
  @Input() label;
  @Input() placeholder;
  @Input() type = 'text';

  constructor() {
  }

  ngOnInit() {

  }
}
