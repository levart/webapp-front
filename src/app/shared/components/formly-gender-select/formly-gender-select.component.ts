import { Component, OnInit } from '@angular/core';
import {FieldType} from '@ngx-formly/core';
import {Observable, of} from 'rxjs';

@Component({
  selector: 'app-formly-gender-select',
  templateUrl: './formly-gender-select.component.html',
  styleUrls: ['./formly-gender-select.component.scss']
})
export class FormlyGenderSelectComponent extends FieldType implements OnInit {
  distributors$: Observable<any> = of([
    {
      value: 'MALE',
      label: 'მამრობითი'
    },
    {
      value: 'FEMALE',
      label: 'მდედრობითი'
    }
  ]);
  constructor() {
    super();
  }

  ngOnInit() {
  }

}
