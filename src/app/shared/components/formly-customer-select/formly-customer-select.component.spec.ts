import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormlyCustomerSelectComponent } from './formly-customer-select.component';

describe('FormlyCustomerSelectComponent', () => {
  let component: FormlyCustomerSelectComponent;
  let fixture: ComponentFixture<FormlyCustomerSelectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormlyCustomerSelectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormlyCustomerSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
