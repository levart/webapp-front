import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {NgModule} from '@angular/core';
import {BrowserModule, DomSanitizer} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FuseProgressBarModule, FuseSidebarModule, FuseThemeOptionsModule} from '@fuse/components';
import {FuseModule} from '@fuse/fuse.module';
import {FuseSharedModule} from '@fuse/shared.module';
import {TranslateModule} from '@ngx-translate/core';
import {NgxsReduxDevtoolsPluginModule} from '@ngxs/devtools-plugin';
import {NgxsLoggerPluginModule} from '@ngxs/logger-plugin';
import {NgxsRouterPluginModule, RouterStateSerializer} from '@ngxs/router-plugin';
import {NgxsStoragePluginModule, StorageOption} from '@ngxs/storage-plugin';
import {NgxsModule} from '@ngxs/store';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {fuseConfig} from './core/fuse-config';
import {LayoutModule} from './core/layout/layout.module';
import {HomeComponent} from './pages/home/home.component';
import {Params, RouterStateSnapshot} from '@angular/router';
import {TokenInterceptorService} from './core/interceptors/token-interceptor/token-interceptor.service';
import {AuthState} from './pages/login/state/auth.state';
import {environment} from 'environments/environment';
import {OverlayModule} from '@impactdk/ngx-overlay';
import {MatIconModule, MatIconRegistry, MatNativeDateModule} from '@angular/material';
import {FormlyModule} from '@ngx-formly/core';
import {FormlyMaterialModule} from '@ngx-formly/material';
import {FormlyPositionSelectComponent} from './shared/components/formly-position-select/formly-position-select.component';
import {MaterialModule} from './shared/modules/material.module';
import {FormlyMatToggleModule} from '@ngx-formly/material/toggle';
import {FormlyMatDatepickerModule} from '@ngx-formly/material/datepicker';
import {FormlyDistributorSelectComponent} from './shared/components/formly-distributor-select/formly-distributor-select.component';
import {FormlyProductCategorySelectComponent} from './shared/components/formly-product-category-select/formly-product-category-select.component';
import {FormlyServiceCategorySelectComponent} from './shared/components/formly-service-category-select/formly-service-category-select.component';
import {FormlyColorPickerComponent} from './shared/components/formly-color-picker/formly-color-picker.component';
import {ColorPickerModule} from 'ngx-color-picker';
import {FormlyEmployeeSelectComponent} from './shared/components/formly-employee-select/formly-employee-select.component';
import {FormlyCustomerSelectComponent} from './shared/components/formly-customer-select/formly-customer-select.component';
import {FormlyRepeatSectionComponent} from './shared/components/formly-repeat-section/formly-repeat-section.component';
import {FormlyServiceSelectComponent} from './shared/components/formly-service-select/formly-service-select.component';
import {FormlyProductsSelectComponent} from './shared/components/formly-products-select/formly-products-select.component';
import {FormlyTimepikerComponent} from './shared/components/formly-timepiker/formly-timepiker.component';
import {NgxMaterialTimepickerModule} from 'ngx-material-timepicker';
import {FormlyInputComponent} from './shared/components/formly-input/formly-input.component';
import {BsDatepickerModule} from '@bit/valor-software.ngx-bootstrap.datepicker';
import {FormlyDatepickerComponent} from './shared/components/formly-datepicker/formly-datepicker.component';
import {FormlyGenderSelectComponent} from './shared/components/formly-gender-select/formly-gender-select.component';
import {FormlySallaryTypeSelectComponent} from './shared/components/formly-sallary-type-select/formly-sallary-type-select.component';

export interface RouterStateParams {
  url: string;
  params: Params;
  queryParams: Params;
}

// Map the router snapshot to { url, params, queryParams }
export class CustomRouterStateSerializer implements RouterStateSerializer<RouterStateParams> {
  serialize(routerState: RouterStateSnapshot): RouterStateParams {
    const {
      url,
      root: {queryParams}
    } = routerState;

    let {root: route} = routerState;
    while (route.firstChild) {
      route = route.firstChild;
    }

    const {params} = route;

    return {url, params, queryParams};
  }
}

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    FormlyPositionSelectComponent,
    FormlyDistributorSelectComponent,
    FormlyProductCategorySelectComponent,
    FormlyServiceCategorySelectComponent,
    FormlyColorPickerComponent,
    FormlyEmployeeSelectComponent,
    FormlyCustomerSelectComponent,
    FormlyRepeatSectionComponent,
    FormlyServiceSelectComponent,
    FormlyProductsSelectComponent,
    FormlyTimepikerComponent,
    FormlyInputComponent,
    FormlyDatepickerComponent,
    FormlyGenderSelectComponent,
    FormlySallaryTypeSelectComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    OverlayModule,
    TranslateModule.forRoot(),
    NgxsModule.forRoot([AuthState], {developmentMode: !environment.production}),
    NgxsLoggerPluginModule.forRoot(),
    NgxsReduxDevtoolsPluginModule.forRoot(),
    NgxsStoragePluginModule.forRoot({
      key: ['auth.token', 'auth.user'],
      storage: StorageOption.SessionStorage
    }),
    NgxsRouterPluginModule.forRoot(),
    FormlyModule.forRoot({
      types: [
        {name: 'wa-input', component: FormlyInputComponent},
        {name: 'positions', component: FormlyPositionSelectComponent},
        {name: 'distributors', component: FormlyDistributorSelectComponent},
        {name: 'productCategories', component: FormlyProductCategorySelectComponent},
        {name: 'serviceCategories', component: FormlyServiceCategorySelectComponent},
        {name: 'colors', component: FormlyColorPickerComponent},
        {name: 'employees', component: FormlyEmployeeSelectComponent},
        {name: 'repeat', component: FormlyRepeatSectionComponent},
        {name: 'services', component: FormlyServiceSelectComponent},
        {name: 'products', component: FormlyProductsSelectComponent},
        {name: 'timepiker', component: FormlyTimepikerComponent},
        {name: 'wa-datepicker', component: FormlyDatepickerComponent},
        {name: 'gender', component: FormlyGenderSelectComponent},
        {name: 'salary-type', component: FormlySallaryTypeSelectComponent},
      ],
    }),
    FormlyMatToggleModule,
    FormlyMaterialModule,
    MatNativeDateModule,
    FormlyMatDatepickerModule,

    // Fuse modules
    FuseModule.forRoot(fuseConfig),
    FuseProgressBarModule,
    FuseSharedModule,
    FuseSidebarModule,
    FuseThemeOptionsModule,

    // App modules
    LayoutModule,
    MatIconModule,
    MaterialModule,
    ColorPickerModule,
    NgxMaterialTimepickerModule,
    BsDatepickerModule.forRoot(),

  ],
  providers: [{provide: HTTP_INTERCEPTORS, useClass: TokenInterceptorService, multi: true}],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(matIconRegistry: MatIconRegistry, domSanitizer: DomSanitizer) {
    matIconRegistry.addSvgIconSet(
      domSanitizer.bypassSecurityTrustResourceUrl('./assets/mdi.svg')
    ); // Or whatever path you placed mdi.svg at
    matIconRegistry.addSvgIcon(
      'realization',
      domSanitizer.bypassSecurityTrustResourceUrl('./assets/icons/realization.svg')
    );
  }
}
