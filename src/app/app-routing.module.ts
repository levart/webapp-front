import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './pages/login/login.component';
import { HomeComponent } from './pages/home/home.component';
import { AuthGuard } from './core/guards/auth.guard';

const routes: Routes = [
  { path: '', redirectTo: '/', pathMatch: 'full' },
  {
    path: 'dashboard',
    component: HomeComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'cashier',
    canActivate: [AuthGuard],
    loadChildren: () => import('./pages/cashier/cashier.module').then(m => m.CashierModule)
  },
  {
    path: 'employees',
    canActivate: [AuthGuard],
    loadChildren: () => import('./pages/employees/employees.module').then(m => m.EmployeesModule)
  },
  {
    path: 'salary',
    canActivate: [AuthGuard],
    loadChildren: () => import('./pages/salary/salary.module').then(m => m.SalaryModule)
  },
  {
    path: 'products',
    canActivate: [AuthGuard],
    loadChildren: () => import('./pages/products/products.module').then(m => m.ProductsModule)
  },
  {
    path: 'realizations',
    canActivate: [AuthGuard],
    loadChildren: () => import('./pages/realizations/realizations.module').then(m => m.RealizationsModule)
  },
  {
    path: 'transactions',
    canActivate: [AuthGuard],
    loadChildren: () => import('./pages/transaction/transaction.module').then(m => m.TransactionModule)
  },
  {
    path: 'customers',
    canActivate: [AuthGuard],
    loadChildren: () => import('./pages/customer/customer.module').then(m => m.CustomerModule)
  },
  {
    path: 'services',
    canActivate: [AuthGuard],
    loadChildren: () => import('./pages/service/service.module').then(m => m.ServiceModule)
  },  {
    path: 'calendars',
    canActivate: [AuthGuard],
    loadChildren: () => import('./pages/calendar/calendar.module').then(m => m.CalendarModule)
  },
  {
    path: '',
    loadChildren: () => import('./pages/login/login.module').then(m => m.LoginModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
